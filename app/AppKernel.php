<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new FOS\RestBundle\FOSRestBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new OldSound\RabbitMqBundle\OldSoundRabbitMqBundle(),
            new Algolia\AlgoliaSearchBundle\AlgoliaAlgoliaSearchBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),

            new Plt\Bundle\ApiBundle\PltApiBundle(),
            new Plt\Bundle\LocationBundle\PltLocationBundle(),
            new Plt\Bundle\UserBundle\PltUserBundle(),
            new Plt\Bundle\MailBundle\PltMailBundle(),
            new Plt\Bundle\AsyncBundle\PltAsyncBundle(),
            new Plt\Bundle\PaymentBundle\PltPaymentBundle(),
            new Plt\Bundle\TransactionBundle\PltTransactionBundle(),
            new Plt\Bundle\EntityBundle\PltEntityBundle(),
            new Plt\Bundle\EventBundle\PltEventBundle(),
            new Plt\Bundle\MediaBundle\PltMediaBundle(),
            new Plt\Bundle\UtilityBundle\PltUtilityBundle(),
            new Plt\Bundle\ReviewBundle\PltReviewBundle(),
            new Plt\Bundle\MessageBundle\PltMessageBundle(),
            new Plt\Bundle\ServiceBundle\PltServiceBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir() . '/config/config_' . $this->getEnvironment() . '.yml');
    }
}
