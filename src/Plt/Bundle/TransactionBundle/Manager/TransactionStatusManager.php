<?php

namespace Plt\Bundle\TransactionBundle\Manager;

use Plt\Bundle\TransactionBundle\Entity\TransactionStatus;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class TransactionStatusManager extends EntityManager
{
    public function getTransactionStatusQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('t');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getTransactionStatus($limit = 10, $offset = 0)
    {
        $tQueryBuilder = $this->getTransactionStatusQueryBuilder($limit, $offset);

        return $tQueryBuilder->getQuery()->execute();
    }

    public function addTransactionStatus($data)
    {
        $status = new TransactionStatus();

        ArrayUtil::toEntity($status, $data);

        $errors = $this->validate($status);
        if (null != $errors) {
            return $errors;
        }

        $this->save($status);

        return $status;
    }

    public function deleteTransactionStatus($transactionStatus)
    {
        $this->delete($transactionStatus);
    }
}
