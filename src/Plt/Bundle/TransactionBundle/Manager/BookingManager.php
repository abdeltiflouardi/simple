<?php

namespace Plt\Bundle\TransactionBundle\Manager;

use Plt\Bundle\TransactionBundle\Entity\BookingLine;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class BookingManager extends EntityManager
{
    public function getBookingQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('b');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getBookings($limit = 10, $offset = 0)
    {
        $bookingQueryBuilder = $this->getBookingQueryBuilder($limit, $offset);

        return $bookingQueryBuilder->getQuery()->execute();
    }

    public function getBookingsByUser($user, $limit = 10, $offset = 0)
    {
        $bookingQueryBuilder = $this->getBookingQueryBuilder($limit, $offset);

        $bookingQueryBuilder
            ->where('b.user = :user')
            ->setParameter('user', VarUtil::toInt($user))
        ;

        return $bookingQueryBuilder->getQuery()->execute();
    }

    public function getBookingsByEntity($entity, $limit = 10, $offset = 0)
    {
        $bookingQueryBuilder = $this->getBookingQueryBuilder($limit, $offset);

        $bookingQueryBuilder
            ->where('b.entity = :entity')
            ->setParameter('entity', VarUtil::toInt($entity))
        ;

        return $bookingQueryBuilder->getQuery()->execute();
    }

    public function getBookingsByIsValid($isValid, $limit = 10, $offset = 0)
    {
        $bookingQueryBuilder = $this->getBookingQueryBuilder($limit, $offset);

        $bookingQueryBuilder
            ->where('b.isValidated = :isValidated')
            ->setParameter('isValidated', $isValid)
        ;

        return $bookingQueryBuilder->getQuery()->execute();
    }

    public function getBookingsByVoucher($voucher, $limit = 10, $offset = 0)
    {
        $bookingQueryBuilder = $this->getBookingQueryBuilder($limit, $offset);

        $bookingQueryBuilder
            ->where('b.voucher = :voucher')
            ->setParameter('voucher', VarUtil::toInt($voucher))
        ;

        return $bookingQueryBuilder->getQuery()->execute();
    }

    public function cancelBooking($user, $booking)
    {
        // @todo cancel transaction;

        return $booking;
    }

    public function doBooking($user, $entity, $data)
    {
        $data['user'] = $user;
        $data['entity'] = $entity;

        $booking = $this->createInstance();

        $this->updateBooking($booking, $data);

        // @todo process transaction
    }

    public function addBooking($user, $data)
    {
        $data['user'] = $user;

        $booking = $this->createInstance();

        $this->updateBooking($booking, $data);
    }

    public function updateBooking($booking, $data)
    {
        ArrayUtil::toEntity($booking, $data);

        $errors = $this->validate($booking);
        if (null != $errors) {
            return $errors;
        }

        $this->save($booking);

        return $booking;
    }

    public function deleteBooking($booking)
    {
        $this->delete($booking);
    }

    public function createBookingLines($booking, $data)
    {
        $data['booking'] = $booking;

        $bookingLine = new BookingLine();

        ArrayUtil::toEntity($bookingLine, $data);

        $errors = $this->validate($bookingLine);
        if (null != $errors) {
            return $errors;
        }

        $this->save($bookingLine);

        return $bookingLine;
    }

    public function deleteBookingLines($booking)
    {
        foreach ($booking->getBookingLines() as $line) {
            $this->delete($line);
        }
    }
}
