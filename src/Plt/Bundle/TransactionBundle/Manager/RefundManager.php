<?php

namespace Plt\Bundle\TransactionBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class RefundManager extends EntityManager
{
    public function getRefundQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('r');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getRefundByUser($user, $limit = 10, $offset = 0)
    {
        $refundQueryBuilder = $this->getRefundQueryBuilder($limit, $offset);

        $refundQueryBuilder
            ->where('r.userTo = :user')
            ->setParameter('user', VarUtil::toInt($user))
        ;

        return $refundQueryBuilder->getQuery()->execute();
    }

    public function getRefundByStatus($status, $limit = 10, $offset = 0)
    {
        $refundQueryBuilder = $this->getRefundQueryBuilder($limit, $offset);

        $refundQueryBuilder
            ->where('r.transactionStatus = :status')
            ->setParameter('status', VarUtil::toInt($status))
        ;

        return $refundQueryBuilder->getQuery()->execute();
    }

    public function addRefund($transactionP2P, $userTo, $mixed)
    {
        $mixed['userTo'] = $userTo;
        $mixed['transactionP2P'] = $transactionP2P;

        $refund = $this->createInstance();

        ArrayUtil::toEntity($refund, $mixed);

        $errors = $this->validate($refund);
        if (null != $errors) {
            return $errors;
        }

        $this->save($refund);

        return $refund;
    }

    public function deleteRefund($refund)
    {
        $this->delete($refund);
    }
}
