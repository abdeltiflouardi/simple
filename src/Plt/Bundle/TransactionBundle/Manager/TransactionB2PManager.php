<?php

namespace Plt\Bundle\TransactionBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class TransactionB2PManager extends EntityManager
{
    public function getTransactionsQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('t');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getTransactionB2PByUserTo($user, $limit = 10, $offset = 0)
    {
        $transactionQueryBuilder = $this->getTransactionsQueryBuilder($limit, $offset);

        $transactionQueryBuilder
            ->where('t.userTo = :user')
            ->setParameter('user', $user)
        ;

        return $transactionQueryBuilder->getQuery()->execute();
    }

    public function getTransactionB2PByStatus($status, $limit = 10, $offset = 0)
    {
        $transactionQueryBuilder = $this->getTransactionsQueryBuilder($limit, $offset);

        $transactionQueryBuilder
            ->where('t.transactionStatus = :status')
            ->setParameter('status', $status)
        ;

        return $transactionQueryBuilder->getQuery()->execute();
    }

    public function addTransactionB2P($userTo, $data)
    {
        $b2p = $this->createInstance();

        ArrayUtil::toEntity($b2p, $data);

        $errors = $this->validate($b2p);
        if (null != $errors) {
            return $errors;
        }

        $this->save($b2p);

        return $b2p;
    }

    public function deleteTransactionB2P($transaction)
    {
        // @todo
    }
}
