<?php

namespace Plt\Bundle\TransactionBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class TransactionP2PManager extends EntityManager
{
    public function getTransactionsQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('t');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getTransactionsP2P($limit = 10, $offset = 0)
    {
        $transactionQueryBuilder = $this->getTransactionsQueryBuilder($limit, $offset);

        return $transactionQueryBuilder->getQuery()->execute();
    }

    public function addTransactionP2P($userFrom, $userTo, $mixed)
    {
        $mixed['userTo'] = $userTo;
        $mixed['userFrom'] = $userFrom;

        $transaction = $this->createInstance();

        ArrayUtil::toEntity($transaction, $mixed);

        $errors = $this->validate($transaction);
        if (null != $errors) {
            return $errors;
        }

        $this->save($transaction);

        return $transaction;
    }

    public function getTransactionByUserFrom($user, $limit = 10, $offset = 0)
    {
        $transactionQueryBuilder = $this->getTransactionsQueryBuilder($limit, $offset);

        $transactionQueryBuilder
            ->where('t.userFrom = :user')
            ->setParameter('user', VarUtil::toInt($user))
        ;

        return $transactionQueryBuilder->getQuery()->execute();
    }

    public function getTransactionByUserTo($user, $limit = 10, $offset = 0)
    {
        $transactionQueryBuilder = $this->getTransactionsQueryBuilder($limit, $offset);

        $transactionQueryBuilder
            ->where('t.userTo = :user')
            ->setParameter('user', VarUtil::toInt($user))
        ;

        return $transactionQueryBuilder->getQuery()->execute();
    }

    public function deleteTransactionP2P($transaction)
    {
        $this->delete($transaction);
    }

    public function getTransactionsByVoucher($voucher, $limit = 10, $offset = 0)
    {
        $transactionQueryBuilder = $this->getTransactionsQueryBuilder($limit, $offset);

        $transactionQueryBuilder
            ->where('t.voucher = :voucher')
            ->setParameter('voucher', VarUtil::toInt($voucher))
        ;

        return $transactionQueryBuilder->getQuery()->execute();
    }

    public function getTransactionsByStatus($status, $limit = 10, $offset = 0)
    {
        $transactionQueryBuilder = $this->getTransactionsQueryBuilder($limit, $offset);

        $transactionQueryBuilder
            ->where('t.transactionStatus = :status')
            ->setParameter('status', VarUtil::toInt($status))
        ;

        return $transactionQueryBuilder->getQuery()->execute();
    }

    public function getTransactionsByEntity($entity, $limit = 10, $offset = 0)
    {
        $transactionQueryBuilder = $this->getTransactionsQueryBuilder($limit, $offset);

        $transactionQueryBuilder
            ->where('t.entity = :entity')
            ->setParameter('entity', VarUtil::toInt($entity))
        ;

        return $transactionQueryBuilder->getQuery()->execute();
    }
}
