<?php

namespace Plt\Bundle\TransactionBundle\Manager;

use Plt\Bundle\TransactionBundle\Entity\TransactionSettings;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class TransactionSettingsManager extends EntityManager
{
    public function getTransactionSettingsQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('t');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getTransactionSettings($limit = 10, $offset = 0)
    {
        $tQueryBuilder = $this->getTransactionSettingsQueryBuilder($limit, $offset);

        return $tQueryBuilder->getQuery()->execute();
    }

    public function addTransactionSettings($data)
    {
        $settings = new TransactionSettings();

        ArrayUtil::toEntity($settings, $data);

        $errors = $this->validate($settings);
        if (null != $errors) {
            return $errors;
        }

        $this->save($settings);

        return $settings;
    }

    public function deleteTransactionSettings($transactionSettings)
    {
        $this->delete($transactionSettings);
    }
}
