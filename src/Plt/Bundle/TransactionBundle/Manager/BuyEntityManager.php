<?php

namespace Plt\Bundle\TransactionBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class BuyEntityManager extends EntityManager
{
    public function getBuyEntityQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('be');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function createBuyEntity($user, $entity, $data)
    {
        $data['user'] = $user;
        $data['entity'] = $entity;

        $buyEntity = $this->createInstance();

        ArrayUtil::toEntity($buyEntity, $data);

        $errors = $this->validate($buyEntity);
        if (null != $errors) {
            return $errors;
        }

        $this->save($buyEntity);

        return $buyEntity;
    }

    public function cancelBuyEntity($user, $buyEntity)
    {
        // @todo process cancellation
        return '@TODO';
    }

    public function doBuyEntity($user, $entity, $data)
    {
        $buyEntity = $this->createBuyEntity($user, $entity, $data);

        // @todo process transaction
        if ($buyEntity instanceof $this->class) {

        }

        return $buyEntity;
    }
}
