<?php

namespace Plt\Bundle\TransactionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Refund
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Refund
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="amount_to_send", type="decimal", scale=2)
     *
     * @Assert\NotBlank()
     */
    private $amountToSend;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_to_id", referencedColumnName="id")
     */
    private $userTo;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\TransactionStatus")
     * @ORM\JoinColumn(name="transaction_status_id", referencedColumnName="id")
     */
    private $transactionStatus;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\TransactionP2P")
     * @ORM\JoinColumn(name="transaction_p2p_id", referencedColumnName="id")
     */
    private $transactionP2P;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amountToSend
     *
     * @param string $amountToSend
     * @return Refund
     */
    public function setAmountToSend($amountToSend)
    {
        $this->amountToSend = $amountToSend;

        return $this;
    }

    /**
     * Get amountToSend
     *
     * @return string
     */
    public function getAmountToSend()
    {
        return $this->amountToSend;
    }

    /**
     * Set userTo
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $userTo
     * @return Refund
     */
    public function setUserTo(\Plt\Bundle\UserBundle\Entity\User $userTo = null)
    {
        $this->userTo = $userTo;

        return $this;
    }

    /**
     * Get userTo
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getUserTo()
    {
        return $this->userTo;
    }

    /**
     * Set transactionStatus
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\TransactionStatus $transactionStatus
     * @return Refund
     */
    public function setTransactionStatus(\Plt\Bundle\TransactionBundle\Entity\TransactionStatus $transactionStatus = null)
    {
        $this->transactionStatus = $transactionStatus;

        return $this;
    }

    /**
     * Get transactionStatus
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\TransactionStatus
     */
    public function getTransactionStatus()
    {
        return $this->transactionStatus;
    }

    /**
     * Set transactionP2P
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\TransactionP2P $transactionP2P
     * @return Refund
     */
    public function setTransactionP2P(\Plt\Bundle\TransactionBundle\Entity\TransactionP2P $transactionP2P = null)
    {
        $this->transactionP2P = $transactionP2P;

        return $this;
    }

    /**
     * Get transactionP2P
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\TransactionP2P
     */
    public function getTransactionP2P()
    {
        return $this->transactionP2P;
    }
}
