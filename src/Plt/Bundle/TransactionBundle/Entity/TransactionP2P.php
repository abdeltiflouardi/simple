<?php

namespace Plt\Bundle\TransactionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TransactionP2P
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class TransactionP2P
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="original_amount_ttc", type="decimal", scale=2)
     *
     * @Assert\NotBlank()
     */
    private $originalAmountTtc;

    /**
     * @ORM\Column(name="final_amount_ttc", type="decimal", scale=2)
     *
     * @Assert\NotBlank()
     */
    private $finalAmountTtc;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_from_id", referencedColumnName="id")
     */
    private $userFrom;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_to_id", referencedColumnName="id")
     */
    private $userTo;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EntityBundle\Entity\Entity")
     */
    private $entity;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\TransactionStatus")
     * @ORM\JoinColumn(name="transaction_status_id", referencedColumnName="id")
     */
    private $transactionStatus;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\Voucher")
     */
    private $voucher;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\Booking")
     */
    private $booking;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\BuyEntity")
     * @ORM\JoinColumn(name="buy_entity_id", referencedColumnName="id")
     */
    private $buyEntity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set originalAmountTtc
     *
     * @param string $originalAmountTtc
     * @return TransactionP2P
     */
    public function setOriginalAmountTtc($originalAmountTtc)
    {
        $this->originalAmountTtc = $originalAmountTtc;

        return $this;
    }

    /**
     * Get originalAmountTtc
     *
     * @return string
     */
    public function getOriginalAmountTtc()
    {
        return $this->originalAmountTtc;
    }

    /**
     * Set finalAmountTtc
     *
     * @param string $finalAmountTtc
     * @return TransactionP2P
     */
    public function setFinalAmountTtc($finalAmountTtc)
    {
        $this->finalAmountTtc = $finalAmountTtc;

        return $this;
    }

    /**
     * Get finalAmountTtc
     *
     * @return string
     */
    public function getFinalAmountTtc()
    {
        return $this->finalAmountTtc;
    }

    /**
     * Set userFrom
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $userFrom
     * @return TransactionP2P
     */
    public function setUserFrom(\Plt\Bundle\UserBundle\Entity\User $userFrom = null)
    {
        $this->userFrom = $userFrom;

        return $this;
    }

    /**
     * Get userFrom
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getUserFrom()
    {
        return $this->userFrom;
    }

    /**
     * Set userTo
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $userTo
     * @return TransactionP2P
     */
    public function setUserTo(\Plt\Bundle\UserBundle\Entity\User $userTo = null)
    {
        $this->userTo = $userTo;

        return $this;
    }

    /**
     * Get userTo
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getUserTo()
    {
        return $this->userTo;
    }

    /**
     * Set entity
     *
     * @param \Plt\Bundle\EntityBundle\Entity\Entity $entity
     * @return TransactionP2P
     */
    public function setEntity(\Plt\Bundle\EntityBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Plt\Bundle\EntityBundle\Entity\Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set transactionStatus
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\TransactionStatus $transactionStatus
     * @return TransactionP2P
     */
    public function setTransactionStatus(\Plt\Bundle\TransactionBundle\Entity\TransactionStatus $transactionStatus = null)
    {
        $this->transactionStatus = $transactionStatus;

        return $this;
    }

    /**
     * Get transactionStatus
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\TransactionStatus
     */
    public function getTransactionStatus()
    {
        return $this->transactionStatus;
    }

    /**
     * Set voucher
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\Voucher $voucher
     * @return TransactionP2P
     */
    public function setVoucher(\Plt\Bundle\TransactionBundle\Entity\Voucher $voucher = null)
    {
        $this->voucher = $voucher;

        return $this;
    }

    /**
     * Get voucher
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\Voucher
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * Set booking
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\Booking $booking
     * @return TransactionP2P
     */
    public function setBooking(\Plt\Bundle\TransactionBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * Set buyEntity
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\BuyEntity $buyEntity
     * @return TransactionP2P
     */
    public function setBuyEntity(\Plt\Bundle\TransactionBundle\Entity\BuyEntity $buyEntity = null)
    {
        $this->buyEntity = $buyEntity;

        return $this;
    }

    /**
     * Get buyEntity
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\BuyEntity
     */
    public function getBuyEntity()
    {
        return $this->buyEntity;
    }
}
