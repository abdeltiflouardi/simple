<?php

namespace Plt\Bundle\TransactionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BuyEntity
 *
 * @ORM\Table(name="BuyEntity", indexes={
 *      @ORM\Index(name="buy_entity_is_validated", columns={"is_validated"})
 * })
 *
 * @ORM\Entity
 */
class BuyEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="price", type="decimal", scale=2)
     *
     * @Assert\NotBlank()
     */
    private $price;

    /**
     * @ORM\Column(name="date", type="date")
     *
     * @Assert\NotBlank()
     */
    private $date;

    /**
     * @ORM\Column(name="is_validated", type="boolean")
     */
    private $isValidated;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EntityBundle\Entity\Entity")
     */
    private $entity;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\Voucher")
     */
    private $voucher;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\TransactionP2P")
     */
    private $transactionP2P;

    public function __construct()
    {
        $this->isValidated = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return BuyEntity
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return BuyEntity
     */
    public function setDate($date)
    {
        if (!$date instanceof \DateTime) {
            try {
                $date = new \DateTime($date);
            } catch (\Exception $e) {
                $date = null;
            }
        }

        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set isValidated
     *
     * @param boolean $isValidated
     * @return BuyEntity
     */
    public function setIsValidated($isValidated)
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    /**
     * Get isValidated
     *
     * @return boolean
     */
    public function getIsValidated()
    {
        return $this->isValidated;
    }

    /**
     * Set entity
     *
     * @param \Plt\Bundle\EntityBundle\Entity\Entity $entity
     * @return BuyEntity
     */
    public function setEntity(\Plt\Bundle\EntityBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Plt\Bundle\EntityBundle\Entity\Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set voucher
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\Voucher $voucher
     * @return BuyEntity
     */
    public function setVoucher(\Plt\Bundle\TransactionBundle\Entity\Voucher $voucher = null)
    {
        $this->voucher = $voucher;

        return $this;
    }

    /**
     * Get voucher
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\Voucher
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * Set transactionP2P
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\TransactionP2P $transactionP2P
     * @return BuyEntity
     */
    public function setTransactionP2P(\Plt\Bundle\TransactionBundle\Entity\TransactionP2P $transactionP2P = null)
    {
        $this->transactionP2P = $transactionP2P;

        return $this;
    }

    /**
     * Get transactionP2P
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\TransactionP2P
     */
    public function getTransactionP2P()
    {
        return $this->transactionP2P;
    }
}
