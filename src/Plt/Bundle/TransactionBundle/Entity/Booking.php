<?php

namespace Plt\Bundle\TransactionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Booking
 *
 * @ORM\Table(name="Booking", indexes={
 *      @ORM\Index(name="booking_date_begin", columns={"date_begin"}),
 *      @ORM\Index(name="booking_date_end", columns={"date_end"}),
 *      @ORM\Index(name="booking_is_validated", columns={"is_validated"}),
 * })
 * @ORM\Entity
 */
class Booking
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="date_begin", type="datetime")
     *
     * @Assert\NotBlank()
     */
    private $dateBegin;

    /**
     * @ORM\Column(name="date_end", type="datetime")
     *
     * @Assert\NotBlank()
     */
    private $dateEnd;

    /**
     * @ORM\Column(name="initial_amount", type="decimal", scale=2)
     *
     * @Assert\NotBlank()
     */
    private $initialAmount;

    /**
     * @ORM\Column(name="final_amount", type="decimal", scale=2)
     *
     * @Assert\NotBlank()
     */
    private $finalAmount;

    /**
     * @ORM\Column(name="is_validated", type="boolean")
     */
    private $isValidated;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EntityBundle\Entity\Entity")
     */
    private $entity;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\TransactionP2P")
     * @ORM\JoinColumn(name="transaction_p2p_id", referencedColumnName="id")
     */
    private $transactionP2P;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\Voucher")
     */
    private $voucher;

    /**
     * @ORM\OneToMany(targetEntity="\Plt\Bundle\TransactionBundle\Entity\BookingLine", mappedBy="booking")
     */
    private $bookingLines;

    public function __construct()
    {
        $this->bookingLines = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isValidated = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateBegin
     *
     * @param \DateTime $dateBegin
     * @return Booking
     */
    public function setDateBegin($dateBegin)
    {
        if (!$dateBegin instanceof \DateTime) {
            try {
                $dateBegin = new \DateTime($dateBegin);
            } catch (\Exception $e) {
                $dateBegin = null;
            }
        }

        $this->dateBegin = $dateBegin;

        return $this;
    }

    /**
     * Get dateBegin
     *
     * @return \DateTime
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return Booking
     */
    public function setDateEnd($dateEnd)
    {
        if (!$dateEnd instanceof \DateTime) {
            try {
                $dateEnd = new \DateTime($dateEnd);
            } catch (\Exception $e) {
                $dateEnd = null;
            }
        }

        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set initialAmount
     *
     * @param string $initialAmount
     * @return Booking
     */
    public function setInitialAmount($initialAmount)
    {
        $this->initialAmount = $initialAmount;

        return $this;
    }

    /**
     * Get initialAmount
     *
     * @return string
     */
    public function getInitialAmount()
    {
        return $this->initialAmount;
    }

    /**
     * Set finalAmount
     *
     * @param string $finalAmount
     * @return Booking
     */
    public function setFinalAmount($finalAmount)
    {
        $this->finalAmount = $finalAmount;

        return $this;
    }

    /**
     * Get finalAmount
     *
     * @return string
     */
    public function getFinalAmount()
    {
        return $this->finalAmount;
    }

    /**
     * Set isValidated
     *
     * @param boolean $isValidated
     * @return Booking
     */
    public function setIsValidated($isValidated)
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    /**
     * Get isValidated
     *
     * @return boolean
     */
    public function getIsValidated()
    {
        return $this->isValidated;
    }

    /**
     * Set user
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $user
     * @return Booking
     */
    public function setUser(\Plt\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set entity
     *
     * @param \Plt\Bundle\EntityBundle\Entity\Entity $entity
     * @return Booking
     */
    public function setEntity(\Plt\Bundle\EntityBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Plt\Bundle\EntityBundle\Entity\Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set transactionP2P
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\TransactionP2P $transactionP2P
     * @return Booking
     */
    public function setTransactionP2P(\Plt\Bundle\TransactionBundle\Entity\TransactionP2P $transactionP2P = null)
    {
        $this->transactionP2P = $transactionP2P;

        return $this;
    }

    /**
     * Get transactionP2P
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\TransactionP2P
     */
    public function getTransactionP2P()
    {
        return $this->transactionP2P;
    }

    /**
     * Set voucher
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\Voucher $voucher
     * @return Booking
     */
    public function setVoucher(\Plt\Bundle\TransactionBundle\Entity\Voucher $voucher = null)
    {
        $this->voucher = $voucher;

        return $this;
    }

    /**
     * Get voucher
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\Voucher
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * Add bookingLines
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\BookingLine $bookingLines
     * @return Booking
     */
    public function addBookingLine(\Plt\Bundle\TransactionBundle\Entity\BookingLine $bookingLines)
    {
        $this->bookingLines[] = $bookingLines;

        return $this;
    }

    /**
     * Remove bookingLines
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\BookingLine $bookingLines
     */
    public function removeBookingLine(\Plt\Bundle\TransactionBundle\Entity\BookingLine $bookingLines)
    {
        $this->bookingLines->removeElement($bookingLines);
    }

    /**
     * Get bookingLines
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBookingLines()
    {
        return $this->bookingLines;
    }
}
