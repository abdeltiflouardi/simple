<?php

namespace Plt\Bundle\TransactionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BookingLine
 *
 * @ORM\Table(name="BookingLine", indexes={
 *      @ORM\Index(name="booking_line_is_validated", columns={"is_validated"})
 * })
 *
 * @ORM\Entity
 */
class BookingLine
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="price", type="decimal", scale=2)
     *
     * @Assert\NotBlank()
     */
    private $price;

    /**
     * @ORM\Column(name="date", type="date")
     *
     * @Assert\NotBlank()
     */
    private $date;

    /**
     * @ORM\Column(name="is_validated", type="boolean")
     */
    private $isValidated;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\TransactionBundle\Entity\Booking")
     */
    private $booking;

    public function __construct()
    {
        $this->isValidated = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return BookingLine
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return BookingLine
     */
    public function setDate($date)
    {
        if (!$date instanceof \DateTime) {
            try {
                $date = new \DateTime($date);
            } catch (\Exception $e) {
                $date = null;
            }
        }

        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set isValidated
     *
     * @param boolean $isValidated
     * @return BookingLine
     */
    public function setIsValidated($isValidated)
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    /**
     * Get isValidated
     *
     * @return boolean
     */
    public function getIsValidated()
    {
        return $this->isValidated;
    }

    /**
     * Set booking
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\Booking $booking
     * @return BookingLine
     */
    public function setBooking(\Plt\Bundle\TransactionBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }
}
