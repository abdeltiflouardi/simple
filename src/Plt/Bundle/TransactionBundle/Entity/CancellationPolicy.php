<?php

namespace Plt\Bundle\TransactionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CancellationPolicy
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CancellationPolicy
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
