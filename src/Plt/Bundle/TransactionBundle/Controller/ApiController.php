<?php

namespace Plt\Bundle\TransactionBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Plt\Bundle\ApiBundle\Controller\BaseApiController as Controller;
use Plt\Bundle\EntityBundle\Entity\Entity;
use Plt\Bundle\TransactionBundle\Entity\Booking;
use Plt\Bundle\TransactionBundle\Entity\BuyEntity;
use Plt\Bundle\TransactionBundle\Entity\Refund;
use Plt\Bundle\TransactionBundle\Entity\TransactionB2P;
use Plt\Bundle\TransactionBundle\Entity\TransactionP2P;
use Plt\Bundle\TransactionBundle\Entity\TransactionSettings;
use Plt\Bundle\TransactionBundle\Entity\TransactionStatus;
use Plt\Bundle\TransactionBundle\Entity\Voucher;
use Plt\Bundle\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /*********************************
     *                               *
     *       API TransactionP2P      *
     *                               *
     *********************************/

    /**
     * @return \Plt\Bundle\TransactionBundle\Manager\TransactionManager;
     */
    public function getTransactionP2PManager()
    {
        return $this->container->get('plt_transaction_p2p.manager');
    }

    /**
     * List all transactionsP2P.
     *
     * @ApiDoc(
     *     section="/transactions",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of transactions.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many transactions to return.")
     *
     * @Annotations\Get("/transactions/p2p")
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getTransactionsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getTransactionP2PManager()->getTransactionsP2P($limit, $offset);
    }

    /**
     * Create new transactionP2P
     *
     * @ApiDoc(
     *      section="/transactions"
     * )
     *
     * @Annotations\Post("/transactions/p2p/users/{user_to_id}/users/{user_from_id}")
     * @Annotations\View()
     *
     * @ParamConverter("userFrom", class="PltUserBundle:User", options={"id": "user_from_id"})
     * @ParamConverter("userTo", class="PltUserBundle:User", options={"id": "user_to_id"})
     */
    public function postTransactionP2PAction($userFrom, $userTo, Request $request)
    {
        $data = $request->get('data');

        $transaction = $this->getTransactionP2PManager()->addTransactionP2P($userFrom, $userTo, $data);

        return $transaction;
    }

    /**
     * Get transactionP2P by userFrom
     *
     * @ApiDoc(
     *      section="/transactions"
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of transactions.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many transactions to return.")
     *
     * @Annotations\Get("/transactions/p2p/users/from/{user_from_id}")
     * @Annotations\View()
     *
     * @ParamConverter("userFrom", class="PltUserBundle:User", options={"id": "user_from_id"})
     */
    public function getTransactionUserFromAction(User $userFrom, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getTransactionP2PManager()->getTransactionByUserFrom($userFrom, $limit, $offset);
    }

    /**
     * Get transactionP2P by userTo
     *
     * @ApiDoc(
     *      section="/transactions"
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of transactions.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many transactions to return.")
     *
     * @Annotations\Get("/transactions/p2p/users/to/{user_to_id}")
     * @Annotations\View()
     *
     * @ParamConverter("userTo", class="PltUserBundle:User", options={"id": "user_to_id"})
     */
    public function getTransactionUserToAction(User $userTo, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getTransactionP2PManager()->getTransactionByUserTo($userTo, $limit, $offset);
    }

    /**
     * Delete transactionP2P
     *
     * @ApiDoc(
     *      section="/transactions"
     * )
     *
     * @Annotations\Delete("/transactions/p2p/{transaction_p2p_id}")
     * @Annotations\View()
     *
     * @ParamConverter("transaction", class="PltTransactionBundle:TransactionP2P", options={"id": "transaction_p2p_id"})
     */
    public function deleteTransactionP2PAction(TransactionP2P $transaction)
    {
        $this->getTransactionP2PManager()->deleteTransactionP2P($transaction);
    }

    /**
     * Get transactionP2P by voucher
     *
     * @ApiDoc(
     *      section="/transactions"
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of transactions.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many transactions to return.")
     *
     * @Annotations\Get("/transactions/p2p/voucher/{voucher_id}")
     * @Annotations\View()
     *
     * @ParamConverter("voucher", class="PltTransactionBundle:Voucher", options={"id": "voucher_id"})
     */
    public function getTransactionVoucherAction(Voucher $voucher, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getTransactionP2PManager()->getTransactionsByVoucher($voucher, $limit, $offset);
    }

    /**
     * Get transactionP2P by status
     *
     * @ApiDoc(
     *      section="/transactions"
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of transactions.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many transactions to return.")
     *
     * @Annotations\Get("/transactions/p2p/statuses/{transaction_status_id}")
     * @Annotations\View()
     *
     * @ParamConverter("status", class="PltTransactionBundle:TransactionStatus", options={"id": "transaction_status_id"})
     */
    public function getTransactionsStatusAction(TransactionStatus $status, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getTransactionP2PManager()->getTransactionsByStatus($status, $limit, $offset);
    }

    /**
     * Get transactionP2P by entity
     *
     * @ApiDoc(
     *      section="/transactions"
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of transactions.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many transactions to return.")
     *
     * @Annotations\Get("/transactions/p2p/entities/{entity_id}")
     * @Annotations\View()
     *
     * @ParamConverter("entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     */
    public function getTransactionsEntityAction(Entity $entity, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getTransactionP2PManager()->getTransactionsByEntity($entity, $limit, $offset);
    }

    /************************************
     *                                  *
     *       API TransactionStatus      *
     *                                  *
     ************************************/
    /**
     * @return \Plt\Bundle\TransactionBundle\Manager\TransactionStatusManager;
     */
    public function getTransactionStatusManager()
    {
        return $this->container->get('plt_transaction_status.manager');
    }

    /**
     * List all transaction status
     *
     * @ApiDoc(
     *     section="/transactions",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of transaction status.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many transaction status to return.")
     *
     * @Annotations\View()
     */
    public function getTransactionsStatusesAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getTransactionStatusManager()->getTransactionStatus($limit, $offset);
    }

    /**
     * Create new transaction status
     *
     * @ApiDoc(
     *      section="/transactions",
     * )
     *
     * @Annotations\View()
     */
    public function postTransactionsStatusAction(Request $request)
    {
        $data = $request->get('data');

        $transactionStatus = $this->getTransactionStatusManager()->addTransactionStatus($data);

        return $transactionStatus;
    }

    /**
     * Delete transaction status
     *
     * @ApiDoc(
     *      section="/transactions",
     * )
     *
     * @Annotations\Delete("/transactions/statuses/{transaction_status_id}")
     * @Annotations\View()
     *
     * @ParamConverter("transactionStatus", class="PltTransactionBundle:TransactionStatus", options={"id": "transaction_status_id"})
     */
    public function deleteTransactionsStatusesAction(TransactionStatus $transactionStatus)
    {
        $this->getTransactionStatusManager()->deleteTransactionStatus($transactionStatus);
    }

    /**************************************
     *                                    *
     *       API TransactionSettings      *
     *                                    *
     **************************************/
    /**
     * @return \Plt\Bundle\TransactionBundle\Manager\TransactionSettingsManager;
     */
    public function getTransactionSettingsManager()
    {
        return $this->container->get('plt_transaction_settings.manager');
    }

    /**
     * List all transaction settings
     *
     * @ApiDoc(
     *     section="/transactions",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of transaction settings.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many transaction settings to return.")
     *
     * @Annotations\View()
     */
    public function getTransactionsSettingsAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getTransactionSettingsManager()->getTransactionSettings($limit, $offset);
    }

    /**
     * Create new transaction settings
     *
     * @ApiDoc(
     *      section="/transactions",
     * )
     *
     * @Annotations\View()
     */
    public function postTransactionsSettingsAction(Request $request)
    {
        $data = $request->get('data');

        $transactionSettings = $this->getTransactionSettingsManager()->addTransactionSettings($data);

        return $transactionSettings;
    }

    /**
     * Delete transaction settings
     *
     * @ApiDoc(
     *      section="/transactions",
     * )
     *
     * @Annotations\Delete("/transactions/settings/{transaction_settings_id}")
     * @Annotations\View()
     *
     * @ParamConverter("transactionSettings", class="PltTransactionBundle:TransactionSettings", options={"id": "transaction_settings_id"})
     */
    public function deleteTransactionsSettingsAction(TransactionSettings $transactionSettings)
    {
        $this->getTransactionSettingsManager()->deleteTransactionSettings($transactionSettings);
    }

    /*********************************
     *                               *
     *       API TransactionB2P      *
     *                               *
     *********************************/
    /**
     * @return \Plt\Bundle\TransactionBundle\Manager\TransactionManager;
     */
    public function getTransactionB2PManager()
    {
        return $this->container->get('plt_transaction_b2p.manager');
    }

    /**
     * List all transaction B2P by user
     *
     * @ApiDoc(
     *     section="/transactions",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of transaction B2P.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many transaction B2P to return.")
     *
     * @Annotations\get("/transactions/b2p/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function getTransactionsB2PUserAction(User $user, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getTransactionB2PManager()->getTransactionB2PByUserTo($user, $limit, $offset);
    }

    /**
     * List all transaction B2P by status
     *
     * @ApiDoc(
     *     section="/transactions",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of transaction B2P.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many transaction B2P to return.")
     *
     * @Annotations\get("/transactions/b2p/statuses/{transaction_status_id}")
     * @Annotations\View()
     *
     * @ParamConverter("transactionStatus", class="PltTransactionBundle:TransactionStatus", options={"id": "transaction_status_id"})
     */
    public function getTransactionsB2PStatusAction(TransactionStatus $transactionStatus, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getTransactionB2PManager()->getTransactionB2PByStatus($transactionStatus, $limit, $offset);
    }

    /**
     * Create new transaction B2P
     *
     * @ApiDoc(
     *      section="/transactions",
     * )
     *
     * @Annotations\Post("/transactions/b2p/users/{user_id}")
     *
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function postTransactionsB2PAction(User $user, Request $request)
    {
        $data = $request->get('data');

        $transactionB2P = $this->getTransactionB2PManager()->addTransactionB2P($user, $data);

        return $transactionB2P;
    }

    /**
     * Delete transaction B2P
     *
     * @ApiDoc(
     *      section="/transactions",
     * )
     *
     * @Annotations\Delete("/transactions/b2p/{transaction_b2p_id}")
     * @Annotations\View()
     *
     * @ParamConverter("transactionB2P", class="PltTransactionBundle:TransactionB2P", options={"id": "transaction_b2p_id"})
     */
    public function deleteTransactionsB2PAction(TransactionB2P $transactionB2P)
    {
        $this->getTransactionB2PManager()->deleteTransactionB2P($transactionB2P);
    }

    /**************************
     *                        *
     *       API Refund       *
     *                        *
     **************************/

    /**
     * @return \Plt\Bundle\TransactionBundle\Manager\RefundManager
     */
    public function getRefundManager()
    {
        return $this->container->get('plt_refund.manager');
    }

    /**
     * List all Refund by user
     *
     * @ApiDoc(
     *     section="/transactions",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of refund.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many refund to return.")
     *
     * @Annotations\get("/transactions/refund/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function getRefundUserAction(User $user, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getRefundManager()->getRefundByUser($user, $limit, $offset);
    }

    /**
     * List all Refund by status
     *
     * @ApiDoc(
     *     section="/transactions",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of refund.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many refund to return.")
     *
     * @Annotations\get("/transactions/refund/status/{transaction_status_id}")
     * @Annotations\View()
     *
     * @ParamConverter("status", class="PltTransactionBundle:TransactionStatus", options={"id": "transaction_status_id"})
     */
    public function getRefundStatusAction(TransactionStatus $status, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getRefundManager()->getRefundByStatus($status, $limit, $offset);
    }

    /**
     * Create new Refund
     *
     * @ApiDoc(
     *      section="/transactions",
     * )
     *
     * @Annotations\Post("/transactions/{transaction_p2p_id}/refund/users/{user_id}")
     *
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     * @ParamConverter("transactionP2P", class="PltTransactionBundle:TransactionP2P", options={"id": "transaction_p2p_id"})
     */
    public function postRefundAction(TransactionP2P $transactionP2P, User $user, Request $request)
    {
        $data = $request->get('data');

        $refund = $this->getRefundManager()->addRefund($transactionP2P, $user, $data);

        return $refund;
    }

    /**
     * Delete refund
     *
     * @ApiDoc(
     *      section="/transactions",
     * )
     *
     * @Annotations\Delete("/transactions/refund/{refund_id}")
     * @Annotations\View()
     *
     * @ParamConverter("refund", class="PltTransactionBundle:Refund", options={"id": "refund_id"})
     */
    public function deleteRefundAction(Refund $refund)
    {
        $this->getRefundManager()->deleteRefund($refund);
    }

    /**************************
     *                        *
     *       API Booking      *
     *                        *
     **************************/

    /**
     * @return \Plt\Bundle\TransactionBundle\Manager\BookingManager
     */
    public function getBookingManager()
    {
        return $this->container->get('plt_booking.manager');
    }

    /**
     * List all booking.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of booking.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many booking to return.")
     *
     * @Annotations\Get("/bookings")
     * @Annotations\View()
     */
    public function getBookingsAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getBookingManager()->getBookings($limit, $offset);
    }

    /**
     * List all booking by user.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of booking.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many booking to return.")
     *
     * @Annotations\Get("/bookings/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function getBookingsUsersAction(User $user, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getBookingManager()->getBookingsByUser($user, $limit, $offset);
    }

    /**
     * List all booking by entity.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of booking.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many booking to return.")
     *
     * @Annotations\Get("/bookings/entities/{entity_id}")
     * @Annotations\View()
     *
     * @ParamConverter("entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     */
    public function getBookingsEntitiesAction(Entity $entity, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getBookingManager()->getBookingsByEntity($entity, $limit, $offset);
    }

    /**
     * List all booking by validation.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of booking.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many booking to return.")
     *
     * @Annotations\Get("/bookings/is-valid/{isvalid}")
     * @Annotations\View()
     *
     */
    public function getBookingIsValidAction($isvalid, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getBookingManager()->getBookingsByIsValid($isvalid, $limit, $offset);
    }

    /**
     * List all booking by voucher.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of booking.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many booking to return.")
     *
     * @Annotations\Get("/bookings/vouchers/{voucher_id}")
     * @Annotations\View()
     *
     * @ParamConverter("voucher", class="PltTransactionBundle:Voucher", options={"id": "voucher_id"})
     */
    public function getBookingsVouchersAction(Voucher $voucher, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getBookingManager()->getBookingsByVoucher($voucher, $limit, $offset);
    }

    /**
     * Create new booking.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\Post("/bookings/users/{user_id}")
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function postBookingAction(User $user, Request $request)
    {
        $data = $request->get('data');

        $booking = $this->getBookingManager()->addBooking($user, $data);

        return $booking;
    }

    /**
     *  Cancel booking.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\Post("/bookings/{booking_id}/cancel")
     *
     * @ParamConverter("booking", class="PltTransactionBundle:Booking", options={"id": "booking_id"})
     */
    public function postBookingCancelAction(Booking $booking)
    {
        $user = $this->getUser();

        return $this->getBookingManager()->cancelBooking($user, $booking);
    }

    /**
     * Update booking.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\Put("/bookings/{booking_id}")
     *
     * @ParamConverter("booking", class="PltTransactionBundle:Booking", options={"id": "booking_id"})
     */
    public function putBookingAction(Booking $booking, Request $request)
    {
        $data = $request->get('data');

        $booking = $this->getBookingManager()->updateBooking($booking, $data);

        return $booking;
    }

    /**
     * Create new booking and do transaction.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\Post("/bookings/users/{user_id}/entities/{entity_id}/process")
     */
    public function postBookingProcessAction(User $user, Entity $entity, Request $request)
    {
        $data = $request->get('data');

        $booking = $this->getBookingManager()->doBooking($user, $data);

        return $booking;
    }

    /**
     * Delete booking.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\Delete("/bookings/{booking_id}")
     *
     * @ParamConverter("booking", class="PltTransactionBundle:Booking", options={"id": "booking_id"})
     */
    public function deleteBookingAction(Booking $booking)
    {
        $this->getBookingManager()->deleteBooking($booking);
    }

    /**
     * Add booking line.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\Post("/bookings/{booking_id}/line")
     *
     * @ParamConverter("booking", class="PltTransactionBundle:Booking", options={"id": "booking_id"})
     */
    public function postBookingLineAction(Booking $booking, Request $request)
    {
        $data = $request->get('data');

        $bookingLine = $this->getBookingManager()->createBookingLines($booking, $data);

        return $bookingLine;
    }

    /**
     * Delete booking line.
     *
     * @ApiDoc(
     *      section="/bookings"
     * )
     *
     * @Annotations\Delete("/bookings/{booking_id}/line")
     *
     * @ParamConverter("booking", class="PltTransactionBundle:Booking", options={"id": "booking_id"})
     */
    public function deleteBookingLineAction(Booking $booking)
    {
        $this->getBookingManager()->deleteBookingLines($booking);
    }

    /*****************************
     *                           *
     *       API BuyEntity       *
     *                           *
     *****************************/
    /**
     * @return \Plt\Bundle\TransactionBundle\Manager\BuyEntityManager;
     */
    public function getBuyEntityManager()
    {
        return $this->container->get('plt_buy_entity.manager');
    }

    /**
     * Create new buy entity.
     *
     * @ApiDoc(
     *      section="/buy-entities"
     * )
     *
     * @Annotations\Post("/buy/entities/{entity_id}/users/{user_id}")
     *
     * @ParamConverter("entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function postBuyEntityAction(User $user, Entity $entity, Request $request)
    {
        $data = $request->get('data');

        $buyEntity = $this->getBuyEntityManager()->createBuyEntity($user, $entity, $data);

        return $buyEntity;
    }

    /**
     * Create new buy entity and process transaction.
     *
     * @ApiDoc(
     *      section="/buy-entities"
     * )
     *
     * @Annotations\Post("/buy/entities/{entity_id}/users/{user_id}/process")
     *
     * @ParamConverter("entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function postProcessBuyEntityAction(User $user, Entity $entity, Request $request)
    {
        $data = $request->get('data');

        $buyEntity = $this->getBuyEntityManager()->doBuyEntity($user, $entity, $data);

        return $buyEntity;
    }

    /**
     * @ApiDoc(
     *      section="/buy-entities"
     * )
     *
     * @Annotations\Post("/buy/entities/{buy_entity_id}/users/{user_id}/cancel")
     *
     * @ParamConverter("buyEntity", class="PltTransactionBundle:BuyEntity", options={"id": "buy_entity_id"})
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function postCancelBuyEntityAction(User $user, BuyEntity $buyEntity)
    {
        return $this->getBuyEntityManager()->cancelBuyEntity($user, $buyEntity);
    }
}
