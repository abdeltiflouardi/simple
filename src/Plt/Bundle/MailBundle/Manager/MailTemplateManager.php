<?php

namespace Plt\Bundle\MailBundle\Manager;

use Plt\Bundle\MailBundle\Entity\MailTemplate;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class MailTemplateManager extends EntityManager
{
    protected $templating;

    public function setTemplating($templating)
    {
        $this->templating = clone $templating;
        $this->templating->setLoader(new \Twig_Loader_String());
    }

    public function getMailTemplatesQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('m');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getMailTemplates($limit = 10, $offset = 0)
    {
        $mailQueryBuilder = $this->getMailTemplatesQueryBuilder($limit, $offset);

        return $mailQueryBuilder->getQuery()->execute();
    }

    public function getTemplateContent($template, $data)
    {
        if (!$this->templating) {
            throw new \Exception("There is no template engine configured.", 1);
        }

        return $this->templating->render($template->getBody(), $data);
    }

    public function addMailTemplate($mixed, $editor)
    {
        $mailTemplate = new MailTemplate();

        return $this->updateMailTemplate($mailTemplate, $mixed, $editor);
    }

    public function deleteMailTemplate($mailTemplate, $editor)
    {
        $this->delete($mailTemplate);
    }

    public function updateMailTemplate($mailTemplate, $mixed, $editor)
    {
        $mixed['last_edited_by'] = $editor;

        ArrayUtil::toEntity($mailTemplate, $mixed);

        $errors = $this->validate($mailTemplate);
        if (null != $errors) {
            return $errors;
        }

        $this->save($mailTemplate);

        return $mailTemplate;
    }
}
