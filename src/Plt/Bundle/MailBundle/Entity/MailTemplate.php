<?php

namespace Plt\Bundle\MailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MailTemplate
 *
 * @ORM\Table(name="MailTemplate", indexes={
 *      @ORM\Index(name="mail_template_type", columns={"type"})
 * })
 * @ORM\Entity
 */
class MailTemplate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="string", length=16)
     *
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @ORM\Column(name="name", type="string", length=64)
     *
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", length=64)
     *
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @ORM\Column(name="subject", type="string", length=255)
     *
     * @Assert\NotBlank()
     */
    private $subject;

    /**
     * @ORM\Column(name="mailfrom", type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Email(checkMX = true, checkHost = true, strict = true)
     */
    private $mailfrom;

    /**
     * @ORM\Column(name="body", type="text")
     *
     * @Assert\NotBlank()
     */
    private $body;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="last_edited_by", referencedColumnName="id")
     *
     * Assert\NotBlank()
     */
    private $lastEditedBy;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return MailTemplate
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MailTemplate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return MailTemplate
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return MailTemplate
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set mailfrom
     *
     * @param string $mailfrom
     * @return MailTemplate
     */
    public function setMailfrom($mailfrom)
    {
        $this->mailfrom = $mailfrom;

        return $this;
    }

    /**
     * Get mailfrom
     *
     * @return string
     */
    public function getMailfrom()
    {
        return $this->mailfrom;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return MailTemplate
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set lastEditedBy
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $lastEditedBy
     * @return MailTemplate
     */
    public function setLastEditedBy(\Plt\Bundle\UserBundle\Entity\User $lastEditedBy = null)
    {
        $this->lastEditedBy = $lastEditedBy;

        return $this;
    }

    /**
     * Get lastEditedBy
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getLastEditedBy()
    {
        return $this->lastEditedBy;
    }
}
