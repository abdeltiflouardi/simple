<?php

namespace Plt\Bundle\MailBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Plt\Bundle\ApiBundle\Controller\BaseApiController as Controller;
use Plt\Bundle\MailBundle\Entity\MailTemplate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**
     * @return Plt\Bundle\MailBundle\Manager\MailTemplateManager
     */
    public function getMailTemplateManager()
    {
        return $this->container->get('plt_mail_template.manager');
    }

    /**
     * List all mail templates.
     *
     * @ApiDoc(
     *     section="/mails",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list mails.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many mails to return.")
     *
     * @Annotations\Get("/mails/templates")
     * @Annotations\View()
     *
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getMailTemplatesAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getMailTemplateManager()->getMailTemplates($limit, $offset);
    }

    /**
     * Get mail template.
     *
     * @ApiDoc(
     *     section="/mails",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Get("/mails/templates/{mail_template_id}")
     * @Annotations\View()
     *
     * @ParamConverter("mailTemplate", class="PltMailBundle:MailTemplate", options={"id": "mail_template_id"})
     */
    public function getMailTemplateAction(MailTemplate $mailTemplate, Request $request)
    {
        $content = $this->getMailTemplateManager()->getTemplateContent($mailTemplate, $request->query->all());

        $mailTemplate->setBody($content);

        return $mailTemplate;
    }

    /**
     * Create Mail template
     *
     * @ApiDoc(
     *     section="/mails",
     *     output="\Plt\Bundle\MailBundle\Entity\MailTemplate",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Post("/mails/templates")
     */
    public function postMailTemplateAction(Request $request)
    {
        $user = $this->getUser();

        $data = $request->get('data');
        $mailTemplate = $this->getMailTemplateManager()->addMailTemplate($data, $user);

        return $mailTemplate;
    }

    /**
     * Update Mail template
     *
     * @ApiDoc(
     *     section="/mails",
     *     output="\Plt\Bundle\MailBundle\Entity\MailTemplate",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Put("/mails/templates/{mail_template_id}")
     *
     * @ParamConverter("mailTemplate", class="PltMailBundle:MailTemplate", options={"id": "mail_template_id"})
     */
    public function putMailTemplateAction(MailTemplate $mailTemplate, Request $request)
    {
        $user = $this->getUser();

        $data = $request->get('data');
        $mailTemplate = $this->getMailTemplateManager()->updateMailTemplate($mailTemplate, $data, $user);

        return $mailTemplate;
    }

    /**
     * Delete mail template
     *
     * @ApiDoc(
     *     section="/mails",
     *     statusCodes={
     *         204="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\Delete("/mails/templates/{mail_template_id}")
     * @Annotations\View()
     *
     * @ParamConverter("mailTemplate", class="PltMailBundle:MailTemplate", options={"id": "mail_template_id"})
     *
     * @return RouteRedirectView
     */
    public function deleteMailTemaplteAction($mailTemplate)
    {
        $user = $this->getUser();

        $this->getMailTemplateManager()->deleteMailTemplate($mailTemplate, $user);
    }
}
