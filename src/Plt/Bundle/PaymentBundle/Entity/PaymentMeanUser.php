<?php

namespace Plt\Bundle\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentMeanUser
 *
 * @ORM\Table(name="PaymentMeanUser", indexes={
 *      @ORM\Index(name="payment_mean_user_is_main", columns={"is_main"})
 * })
 *
 * @ORM\Entity
 */
class PaymentMeanUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="is_main", type="boolean")
     */
    private $isMain;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\PaymentBundle\Entity\PaymentMean")
     * @ORM\JoinColumn(name="payment_mean_id", referencedColumnName="id")
     */
    private $paymentMean;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isMain
     *
     * @param boolean $isMain
     * @return PaymentMeanUser
     */
    public function setIsMain($isMain)
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * Get isMain
     *
     * @return boolean 
     */
    public function getIsMain()
    {
        return $this->isMain;
    }

    /**
     * Set user
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $user
     * @return PaymentMeanUser
     */
    public function setUser(\Plt\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Plt\Bundle\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set paymentMean
     *
     * @param \Plt\Bundle\PaymentBundle\Entity\PaymentMean $paymentMean
     * @return PaymentMeanUser
     */
    public function setPaymentMean(\Plt\Bundle\PaymentBundle\Entity\PaymentMean $paymentMean = null)
    {
        $this->paymentMean = $paymentMean;

        return $this;
    }

    /**
     * Get paymentMean
     *
     * @return \Plt\Bundle\PaymentBundle\Entity\PaymentMean 
     */
    public function getPaymentMean()
    {
        return $this->paymentMean;
    }
}
