<?php

namespace Plt\Bundle\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BankAccount
 *
 * @ORM\Table(name="BankAccount", indexes={
 *      @ORM\Index(name="bank_account_is_main", columns={"is_main"})
 * })
 * @ORM\Entity
 */
class BankAccount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="iban", type="string", length=128)
     *
     * @Assert\NotBlank()
     */
    private $iban;

    /**
     * @ORM\Column(name="bic", type="string", length=32)
     *
     * @Assert\NotBlank()
     */
    private $bic;

    /**
     * @ORM\Column(name="token", type="string", length=255)
     *
     * @Assert\NotBlank()
     */
    private $token;

    /**
     * @ORM\Column(name="owner_name", type="string", length=64)
     *
     * @Assert\NotBlank()
     */
    private $ownerName;

    /**
     * @ORM\Column(name="is_main", type="boolean")
     */
    private $isMain;

    /**
     * @ORM\Column(name="is_archived", type="boolean")
     */
    private $isArchived;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\Address")
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\PaymentBundle\Entity\BankAccountType")
     * @ORM\JoinColumn(name="bank_account_type_id", referencedColumnName="id")
     */
    private $bankAccountType;

    public function __construct()
    {
        $this->isMain = false;
        $this->isArchived = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iban
     *
     * @param string $iban
     * @return BankAccount
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set bic
     *
     * @param string $bic
     * @return BankAccount
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * Get bic
     *
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return BankAccount
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set ownerName
     *
     * @param string $ownerName
     * @return BankAccount
     */
    public function setOwnerName($ownerName)
    {
        $this->ownerName = $ownerName;

        return $this;
    }

    /**
     * Get ownerName
     *
     * @return string
     */
    public function getOwnerName()
    {
        return $this->ownerName;
    }

    /**
     * Set isMain
     *
     * @param boolean $isMain
     * @return BankAccount
     */
    public function setIsMain($isMain)
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * Get isMain
     *
     * @return boolean
     */
    public function getIsMain()
    {
        return $this->isMain;
    }

    /**
     * Set isArchived
     *
     * @param boolean $isArchived
     * @return BankAccount
     */
    public function setIsArchived($isArchived)
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    /**
     * Get isArchived
     *
     * @return boolean
     */
    public function getIsArchived()
    {
        return $this->isArchived;
    }

    /**
     * Set user
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $user
     * @return BankAccount
     */
    public function setUser(\Plt\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set address
     *
     * @param \Plt\Bundle\UserBundle\Entity\Address $address
     * @return BankAccount
     */
    public function setAddress(\Plt\Bundle\UserBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set bankAccountType
     *
     * @param \Plt\Bundle\PaymentBundle\Entity\BankAccountType $bankAccountType
     * @return BankAccount
     */
    public function setBankAccountType(\Plt\Bundle\PaymentBundle\Entity\BankAccountType $bankAccountType = null)
    {
        $this->bankAccountType = $bankAccountType;

        return $this;
    }

    /**
     * Get bankAccountType
     *
     * @return \Plt\Bundle\PaymentBundle\Entity\BankAccountType
     */
    public function getBankAccountType()
    {
        return $this->bankAccountType;
    }
}
