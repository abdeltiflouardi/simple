<?php

namespace Plt\Bundle\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PaymentMean
 *
 * @ORM\Table(name="PaymentMean", indexes={
 *      @ORM\Index(name="payment_mean_is_valid", columns={"is_valid"})
 * })
 * @ORM\Entity
 */
class PaymentMean
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="card_token", type="string", length=128)
     *
     * @Assert\NotBlank
     */
    private $cardToken;

    /**
     * @ORM\Column(name="last_digits", type="integer", length=4)
     *
     * @Assert\NotBlank
     */
    private $lastDigits;

    /**
     * @ORM\Column(name="exp_date", type="string", length=6)
     *
     * @Assert\NotBlank
     */
    private $expDate;

    /**
     * @ORM\Column(name="card_type", type="string", length=16)
     *
     * @Assert\NotBlank
     */
    private $cardType;

    /**
     * @ORM\Column(name="is_valid", type="boolean")
     */
    private $isValid;

    /**
     * @ORM\Column(name="type_payment", type="string", length=32)
     *
     * @Assert\NotBlank
     */
    private $typePayment;

    /**
     * @ORM\Column(name="code", type="string", length=10)
     *
     * @Assert\NotBlank
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\Address")
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\LocationBundle\Entity\Country")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UtilityBundle\Entity\Currency")
     */
    private $currency;

    /**
     * @ORM\OneToMany(targetEntity="\Plt\Bundle\PaymentBundle\Entity\PaymentMeanUser", mappedBy="paymentMean")
     */
    private $paymentMeanUser;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->paymentMeanUser = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isValid = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cardToken
     *
     * @param string $cardToken
     * @return PaymentMean
     */
    public function setCardToken($cardToken)
    {
        $this->cardToken = $cardToken;

        return $this;
    }

    /**
     * Get cardToken
     *
     * @return string
     */
    public function getCardToken()
    {
        return $this->cardToken;
    }

    /**
     * Set lastDigits
     *
     * @param integer $lastDigits
     * @return PaymentMean
     */
    public function setLastDigits($lastDigits)
    {
        $this->lastDigits = $lastDigits;

        return $this;
    }

    /**
     * Get lastDigits
     *
     * @return integer
     */
    public function getLastDigits()
    {
        return $this->lastDigits;
    }

    /**
     * Set expDate
     *
     * @param string $expDate
     * @return PaymentMean
     */
    public function setExpDate($expDate)
    {
        $this->expDate = $expDate;

        return $this;
    }

    /**
     * Get expDate
     *
     * @return string
     */
    public function getExpDate()
    {
        return $this->expDate;
    }

    /**
     * Set cardType
     *
     * @param string $cardType
     * @return PaymentMean
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;

        return $this;
    }

    /**
     * Get cardType
     *
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * Set isValid
     *
     * @param boolean $isValid
     * @return PaymentMean
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid
     *
     * @return boolean
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * Set typePayment
     *
     * @param string $typePayment
     * @return PaymentMean
     */
    public function setTypePayment($typePayment)
    {
        $this->typePayment = $typePayment;

        return $this;
    }

    /**
     * Get typePayment
     *
     * @return string
     */
    public function getTypePayment()
    {
        return $this->typePayment;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return PaymentMean
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set address
     *
     * @param \Plt\Bundle\UserBundle\Entity\Address $address
     * @return PaymentMean
     */
    public function setAddress(\Plt\Bundle\UserBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set country
     *
     * @param \Plt\Bundle\LocationBundle\Entity\Country $country
     * @return PaymentMean
     */
    public function setCountry(\Plt\Bundle\LocationBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Plt\Bundle\LocationBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set currency
     *
     * @param \Plt\Bundle\UtilityBundle\Entity\Currency $currency
     * @return PaymentMean
     */
    public function setCurrency(\Plt\Bundle\UtilityBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Plt\Bundle\UtilityBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Add paymentMeanUser
     *
     * @param \Plt\Bundle\PaymentBundle\Entity\PaymentMeanUser $paymentMeanUser
     * @return PaymentMean
     */
    public function addPaymentMeanUser(\Plt\Bundle\PaymentBundle\Entity\PaymentMeanUser $paymentMeanUser)
    {
        $this->paymentMeanUser[] = $paymentMeanUser;

        return $this;
    }

    /**
     * Remove paymentMeanUser
     *
     * @param \Plt\Bundle\PaymentBundle\Entity\PaymentMeanUser $paymentMeanUser
     */
    public function removePaymentMeanUser(\Plt\Bundle\PaymentBundle\Entity\PaymentMeanUser $paymentMeanUser)
    {
        $this->paymentMeanUser->removeElement($paymentMeanUser);
    }

    /**
     * Get paymentMeanUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaymentMeanUser()
    {
        return $this->paymentMeanUser;
    }
}
