<?php

namespace Plt\Bundle\PaymentBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Plt\Bundle\ApiBundle\Controller\BaseApiController as Controller;
use Plt\Bundle\PaymentBundle\Entity\BankAccount;
use Plt\Bundle\PaymentBundle\Entity\BankAccountType;
use Plt\Bundle\PaymentBundle\Entity\PaymentMean;
use Plt\Bundle\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{

    /*********************
     *                   *
     *  API PaymentMean  *
     *                   *
     *********************/

    /**
     * @return Plt\Bundle\PaymentBundle\Manager\PaymentMeanManager
     */
    public function getPaymentMeanManager()
    {
        return $this->container->get('plt_payment_mean.manager');
    }

    /**
     * Get PaymentMean
     *
     * @ApiDoc(
     *     section="/payments",
     *     statusCodes={
     *         204="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\Get("/payments/{payment_id}")
     * @Annotations\View()
     *
     * @ParamConverter("paymentMean", class="PltPaymentBundle:PaymentMean", options={"id": "payment_id"})
     *
     * @return RouteRedirectView
     */
    public function getPaymentMeanAction(PaymentMean $paymentMean)
    {
        return $paymentMean;
    }

    /**
     * Create PaymentMean
     *
     * @ApiDoc(
     *     section="/payments",
     *     output="\Plt\Bundle\PaymentBundle\Entity\PaymentMean",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Post("/payments")
     *
     */
    public function postPaymentMeanAction(Request $request)
    {
        $data = $request->get('data');
        $paymentMean = $this->getPaymentMeanManager()->addPaymentMean($data);

        return $paymentMean;
    }

    /**
     * Delete PaymentMean
     *
     * @ApiDoc(
     *     section="/payments",
     *     statusCodes={
     *         204="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\Delete("/payments/{payment_id}")
     * @Annotations\View()
     *
     * @ParamConverter("paymentMean", class="PltPaymentBundle:PaymentMean", options={"id": "payment_id"})
     *
     * @return RouteRedirectView
     */
    public function deletePaymentMeanAction(PaymentMean $paymentMean)
    {
        $user = $this->getUser();

        $this->getPaymentMeanManager()->deletePaymentMean($paymentMean, $user);
    }

    /*************************
     *                       *
     *  API PaymentMeanUser  *
     *                       *
     *************************/

    /**
     * @return Plt\Bundle\PaymentBundle\Manager\PaymentMeanUserManager
     */
    public function getPaymentMeanUserManager()
    {
        return $this->container->get('plt_payment_mean_user.manager');
    }

    /**
     * Attach user to payment
     *
     * @ApiDoc(
     *     section="/payments",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Post("/payments/{payment_id}/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     * @ParamConverter("paymentMean", class="PltPaymentBundle:PaymentMean", options={"id": "payment_id"})
     *
     */
    public function postPaymentMeanUserAction(PaymentMean $paymentMean, User $user, Request $request)
    {
        $data = $request->get('data');

        $pmUser = $this->getPaymentMeanUserManager()->addPaymentMeanByUser($paymentMean, $user, $data);

        return $pmUser;
    }

    /**
     * Get all PaymentMean by User
     *
     * @ApiDoc(
     *     section="/payments",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list payment.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many payment to return.")
     *
     * @Annotations\Get("/payments/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function getPaymentMeanUserAction(User $user, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getPaymentMeanManager()->getPaymentMeanByUser($user, $limit, $offset);
    }

    /**
     * Get all Main PaymentMean by User
     *
     * @ApiDoc(
     *     section="/payments",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Get("/payments/users/{user_id}/main")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function getMainPaymentMeanUserAction(User $user)
    {
        $main = $this->getPaymentMeanManager()->getMainPaymentMeanByUser($user);

        return $main;
    }

    /**
     * Set main payment for user
     *
     * @ApiDoc(
     *     section="/payments",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Put("/payments/{payment_id}/users/{user_id}/main")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     * @ParamConverter("paymentMean", class="PltPaymentBundle:PaymentMean", options={"id": "payment_id"})
     */
    public function putMainPaymentMeanUserAction(PaymentMean $paymentMean, User $user)
    {
        $main = $this->getPaymentMeanUserManager()->setMainPaymentMeanByUser($user, $paymentMean);

        return $main;
    }

    /**
     * Detach payment for user
     *
     * @ApiDoc(
     *     section="/payments",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Delete("/payments/{payment_id}/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     * @ParamConverter("paymentMean", class="PltPaymentBundle:PaymentMean", options={"id": "payment_id"})
     */
    public function deletePaymentMeanUserAction(PaymentMean $paymentMean, User $user)
    {
        $this->getPaymentMeanUserManager()->deletePaymentMean($paymentMean, $user);
    }

    /*********************
     *                   *
     *  API BankAccount  *
     *                   *
     *********************/

    /**
     * @return Plt\Bundle\PaymentBundle\Manager\BankAccountManager
     */
    public function getBankAccountManager()
    {
        return $this->container->get('plt_bank_account.manager');
    }

    /**
     * List all banks by user.
     *
     * @ApiDoc(
     *     section="/banks",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Get("/banks/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     *
     * @return array
     */
    public function getBanksAction(User $user)
    {
        return $this->getBankAccountManager()->getBankAccount($user);
    }

    /**
     * Get main bank by user.
     *
     * @ApiDoc(
     *     section="/banks",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Get("/banks/users/{user_id}/main")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     *
     * @return array
     */
    public function getBanksMainAction(User $user)
    {
        return $this->getBankAccountManager()->getMainBankAccount($user);
    }

    /**
     * Create BankAccount
     *
     * @ApiDoc(
     *     section="/banks",
     *     output="\Plt\Bundle\PaymentBundle\Entity\BankAccount",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Post("/banks/users/{user_id}")
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     *
     */
    public function postBankAccountAction(User $user, Request $request)
    {
        $data = $request->get('data');
        $bankAccount = $this->getBankAccountManager()->addBankAccount($data, $user);

        return $bankAccount;
    }

    /**
     * Delete BankAccount
     *
     * @ApiDoc(
     *     section="/banks",
     *     statusCodes={
     *         204="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\Delete("/banks/{bank_id}")
     * @Annotations\View()
     *
     * @ParamConverter("bankAccount", class="PltPaymentBundle:BankAccount", options={"id": "bank_id"})
     *
     * @return RouteRedirectView
     */
    public function deleteBankAccountAction(BankAccount $bankAccount)
    {
        $user = $this->getUser();

        $this->getBankAccountManager()->deleteBankAccount($bankAccount, $user);
    }

    /************************
     *                      *
     *  API BankAccountType *
     *                      *
     ************************/

    /**
     * @return Plt\Bundle\PaymentBundle\Manager\BankAccountTypeManager
     */
    public function getBankAccountTypeManager()
    {
        return $this->container->get('plt_bank_account_type.manager');
    }

    /**
     * List all bank types.
     *
     * @ApiDoc(
     *     section="/banks",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list bank types.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many bank types to return.")
     *
     * @Annotations\Get("/banks/types")
     * @Annotations\View()
     *
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getBankTypesAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getBankAccountTypeManager()->getBankAccountTypes($limit, $offset);
    }

    /**
     * Create BankAccount Type
     *
     * @ApiDoc(
     *     section="/banks",
     *     output="\Plt\Bundle\PaymentBundle\Entity\BankAccountType",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\Post("/banks/types")
     *
     */
    public function postBankAccountTypeAction(Request $request)
    {
        $user = $this->getUser();

        $data = $request->get('data');
        $bankAccountType = $this->getBankAccountTypeManager()->addBankAccountType($data, $user);

        return $bankAccountType;
    }

    /**
     * Delete BankAccount Type
     *
     * @ApiDoc(
     *     section="/banks",
     *     statusCodes={
     *         204="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\Delete("/banks/types/{bank_type_id}")
     * @Annotations\View()
     *
     * @ParamConverter("bankAccountType", class="PltPaymentBundle:BankAccountType", options={"id": "bank_type_id"})
     *
     * @return RouteRedirectView
     */
    public function deleteBankAccountTypeAction(BankAccountType $bankAccountType)
    {
        $user = $this->getUser();

        $this->getBankAccountTypeManager()->deleteBankAccountType($bankAccountType, $user);
    }

}
