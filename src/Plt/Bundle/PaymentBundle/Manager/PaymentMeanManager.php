<?php

namespace Plt\Bundle\PaymentBundle\Manager;

use Plt\Bundle\PaymentBundle\Entity\PaymentMean;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class PaymentMeanManager extends EntityManager
{
    public function getQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('p');
        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getPaymentMeanByUser($user, $limit = 10, $offset = 0)
    {
        $qb = $this->getQueryBuilder($limit, $offset);
        $qb
            ->join('p.paymentMeanUser', 'pmu')

            ->where('pmu.user = :user')
            ->setParameter('user', VarUtil::toInt($user))
        ;

        return $qb->getQuery()->execute();
    }

    public function getMainPaymentMeanByUser($user)
    {
        $qb = $this->repository->createQueryBuilder('p');
        $qb
            ->join('p.paymentMeanUser', 'pmu')

            ->where('pmu.user = :user')
            ->setParameter('user', VarUtil::toInt($user))

            ->andWhere('pmu.isMain = :isMain')
            ->setParameter('isMain', true)
        ;

        return $qb->getQuery()->execute();
    }

    public function addPaymentMean($mixed)
    {
        $paymentMean = new PaymentMean();
        ArrayUtil::toEntity($paymentMean, $mixed);

        $errors = $this->validate($paymentMean);
        if (null != $errors) {
            return $errors;
        }

        $this->save($paymentMean);

        return $paymentMean;
    }

    public function getPaymentMeanById($paymentMeanId)
    {
        return $this->repository->find($paymentMeanId);
    }

    public function deletePaymentMean($paymentMean)
    {
        $this->delete($paymentMean);
    }
}
