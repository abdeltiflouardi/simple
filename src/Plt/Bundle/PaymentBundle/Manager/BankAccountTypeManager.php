<?php

namespace Plt\Bundle\PaymentBundle\Manager;

use Plt\Bundle\PaymentBundle\Entity\BankAccountType;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class BankAccountTypeManager extends EntityManager
{
    public function getBankTypesQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('t');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getBankAccountTypes($limit = 10, $offset = 0)
    {
        $qb = $this->getBankTypesQueryBuilder($limit, $offset);

        return $qb->getQuery()->execute();
    }

    public function addBankAccountType($mixed, $user)
    {
        $bankAccountType = new BankAccountType();
        ArrayUtil::toEntity($bankAccountType, $mixed);

        $errors = $this->validate($bankAccountType);
        if (null != $errors) {
            return $errors;
        }

        $this->save($bankAccountType);

        return $bankAccountType;
    }

    public function deleteBankAccountType($bankAccountType, $user)
    {
        $this->delete($bankAccountType);
    }
}
