<?php

namespace Plt\Bundle\PaymentBundle\Manager;

use Plt\Bundle\PaymentBundle\Entity\PaymentMeanUser;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class PaymentMeanUserManager extends EntityManager
{
    public function getQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('p');
        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getPaymentMeanUser($user, $paymentMean)
    {
        $qb = $this->getQueryBuilder(1, 0);
        $qb
            ->where('p.user = :user')
            ->setParameter('user', VarUtil::toInt($user))

            ->andWhere('p.paymentMean = :paymentMean')
            ->setParameter('paymentMean', VarUtil::toInt($paymentMean))
        ;

        return $qb->getQuery()->getSingleResult();
    }

    public function getPaymentMeanByUser($user, $limit = 10, $offset = 0)
    {
        $qb = $this->getQueryBuilder($limit, $offset);
        $qb
            ->where('p.user = :user')
            ->setParameter('user', VarUtil::toInt($user))
        ;

        return $qb->getQuery()->execute();
    }

    public function setMainPaymentMeanByUser($user, $paymentMean)
    {
        $paymentMeanUser = $this->getPaymentMeanUser($user, $paymentMean);

        if ($paymentMeanUser) {
            $paymentMeanUser->setIsMain(true);

            $this->save($paymentMeanUser);
        }

        return $paymentMeanUser;
    }

    public function deletePaymentMean($paymentMean, $user)
    {
        $paymentMeanUser = $this->getPaymentMeanUser($user, $paymentMean);

        if ($paymentMeanUser) {
            $this->delete($paymentMeanUser);
        }
    }

    public function addPaymentMeanByUser($paymentMean, $user, $mixed)
    {
        $paymentMeanUser = $this->getPaymentMeanUser($user, $paymentMean);
        if (!$paymentMeanUser) {
            $paymentMeanUser = new PaymentMeanUser();

            $paymentMeanUser
                ->setPaymentMean($paymentMean)
                ->setUser($user)
            ;
        }

        ArrayUtil::toEntity($paymentMeanUser, $mixed);

        $this->save($paymentMeanUser);

        return $paymentMeanUser;
    }
}
