<?php

namespace Plt\Bundle\PaymentBundle\Manager;

use Plt\Bundle\PaymentBundle\Entity\BankAccount;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class BankAccountManager extends EntityManager
{
    public function getBankAccountQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('ba');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getBankAccount($user)
    {
        $qb = $this->repository->createQueryBuilder('ba');
        $qb
            ->where('ba.user = :user')
            ->setParameter('user', $user)
        ;

        return $qb->getQuery()->execute();
    }

    public function getMainBankAccount($user)
    {
        $qb = $this->repository->createQueryBuilder('ba');
        $qb
            ->where('ba.user = :user')
            ->setParameter('user', $user)

            ->andWhere('ba.isMain = :isMain')
            ->setParameter('isMain', true)
        ;

        return $qb->getQuery()->execute();
    }

    public function addBankAccount($mixed, $user)
    {
        $mixed['user'] = $user;

        $bankAccount = new BankAccount();
        ArrayUtil::toEntity($bankAccount, $mixed);

        $errors = $this->validate($bankAccount);
        if (null != $errors) {
            return $errors;
        }

        $this->save($bankAccount);

        return $bankAccount;
    }

    public function deleteBankAccount($bankAccount, $user)
    {
        $this->delete($bankAccount);
    }
}
