<?php

namespace Plt\Bundle\ReviewBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReviewLine
 *
 * @ORM\Table(name="ReviewLine", indexes={
 *      @ORM\Index(name="review_line_rating", columns={"rating"})
 * })
 * @ORM\Entity
 */
class ReviewLine
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="rating", type="float")
     */
    private $rating;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\ReviewBundle\Entity\Review")
     */
    private $review;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\ReviewBundle\Entity\ReviewCriteria")
     */
    private $reviewCriteria;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
