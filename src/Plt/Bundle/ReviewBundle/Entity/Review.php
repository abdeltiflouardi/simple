<?php

namespace Plt\Bundle\ReviewBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Review
 *
 * @ORM\Table(name="Review", indexes={
 *      @ORM\Index(name="review_slug", columns={"slug"}),
 *      @ORM\Index(name="review_rating", columns={"rating"})
 * })
 * @ORM\Entity
 */
class Review
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", length=64)
     */
    private $slug;

    /**
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @ORM\Column(name="rating", type="float")
     */
    private $rating;

    /**
     * @ORM\Column(name="status", type="string", length=32)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="posted_by", referencedColumnName="id")
     */
    private $postedBy;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EventBundle\Entity\Event")
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EntityBundle\Entity\Entity")
     */
    private $entity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $user
     * @return Review
     */
    public function setUser(\Plt\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set event
     *
     * @param \Plt\Bundle\EventBundle\Entity\Event $event
     * @return Review
     */
    public function setEvent(\Plt\Bundle\EventBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \Plt\Bundle\EventBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set entity
     *
     * @param \Plt\Bundle\ShareableBundle\Entity\Entity $entity
     * @return Review
     */
    public function setEntity(\Plt\Bundle\ShareableBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Plt\Bundle\ShareableBundle\Entity\Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }
}
