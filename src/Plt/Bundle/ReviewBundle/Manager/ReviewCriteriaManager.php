<?php

namespace Plt\Bundle\ReviewBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;

class ReviewCriteriaManager extends EntityManager
{
    public function getReviewCriteriaQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('rc');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function addReviewCriteria($mixed, $user)
    {

    }

    public function removeReviewCriteria($reviewCriteria, $user)
    {

    }

    public function updateReviewCriteria($reviewCriteria, $user)
    {

    }
}
