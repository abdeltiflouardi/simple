<?php

namespace Plt\Bundle\ReviewBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;

class ReviewManager extends EntityManager
{
    public function getReviewQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('rc');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getReviewsByUser($user, $status)
    {

    }

    public function getReviewsForUser($user, $status)
    {

    }

    public function getReviewsForEntity($entity, $status)
    {

    }

    public function getReviewsForEvent($event, $status)
    {

    }

    public function addReview($mixed, $user)
    {

    }

    public function removeReview($review, $user)
    {

    }
    public function updateReview($review, $mixed, $user)
    {

    }

    public function addReviewLine($mixed, $user)
    {

    }
}
