<?php

namespace Plt\Bundle\ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AskedService
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AskedService
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
