<?php

namespace Plt\Bundle\UtilityBundle\Helper;

use Imagick;
use Plt\Bundle\MediaBundle\Media\Image;

class ImageProcessor
{
    const JPEG_QUALITY = 90;

    const SIZE_SET_SQUARED = 0;
    const SIZE_SET_PORTRAIT = 10;
    const SIZE_SET_WIDE = 20;
    const SIZE_SET_ALL = 30;
    const SIZE_SET_RATIO = 40;

    protected $original_file;
    protected $override;
    protected $save_handler;

    public function __construct($original_file, $save_handler, $override = false)
    {
        $this->original_file = $original_file;
        $this->override = $override;
        $this->save_handler = $save_handler;
    }

    /**
     * Process image, creating useful subset of sizes.
     *
     * @param array|int $size_set Size set or array of size sets, using only
     *                            constants provided by this class (SIZE_SET_X).
     * @param string $destination optional, destination file, fallbacks on the
     * original file name.
     */
    public function process($size_set, $destination = null)
    {
        if (!$destination) {
            $destination = $this->original_file;
        }

        if (!is_callable($this->save_handler)) {
            throw new \InvalidArgumentException("This save handler is not callable");
        }

        $sizes = self::getSizes($size_set);
        if (empty($sizes)) {
            return;
        }

        # Get parts of the file names
        $dir = dirname($destination);
        $name = basename($destination);
        # Image key is the part of the basename that end before the first dot
        $key = strstr($name, '.', true);
        # Image extension is the last part of the basename
        $ext = strrchr($name, '.');

        $origin = new Imagick($this->original_file);

        foreach ($sizes as $size => $params) {
            if (empty($params)) {
                # Invalid image size
                continue;
            }

            $img = clone $origin;
            self::resize($img, $params);

            $img->setImageCompression(Imagick::COMPRESSION_JPEG);
            $img->setImageCompressionQuality(self::JPEG_QUALITY);
            $img->stripImage();

            $new_filename = "$dir/$key.$size$ext";
            call_user_func($this->save_handler, $img, $new_filename);

            $img->destroy();
        }
    }

    /**
     * Apply resize transformations to an Imagick object.
     *
     * @param Imagick $img
     * @param array $params contains a width or an height
     */
    public static function resize(Imagick $img, $params)
    {
        # Fixed size
        if (isset($params['width']) && isset($params['height'])) {
            if ($img->getImageWidth() > $img->getImageHeight() and
                $params['width'] <= $params['height']
            ) {
                // Wide image : lets crop in the center
                $img->resizeImage(0, $params['height'], Imagick::FILTER_LANCZOS, 1);
                $img->cropImage($params['width'], $params['height'],
                    floor($img->getImageWidth() / 2 - ($params['width'] / 2)), 0);
            } else {
                // Portrait/Cover : lets crop on top
                $img->resizeImage($params['width'], 0, Imagick::FILTER_LANCZOS, 1);
                $img->cropImage($params['width'], $params['height'], 0, 0);
            }
        }
        # Ratio with fixed width
        else if (isset($params['width'])) {
            $img->resizeImage($params['width'], 0, Imagick::FILTER_LANCZOS, 1);
        }
        # Ratio with fixed height
        else if (isset($params['height'])) {
            $img->resizeImage(0, $params['height'], Imagick::FILTER_LANCZOS, 1);
        }
    }

    protected static function getSizes($size_set)
    {
        if (is_int($size_set)) {
            $size_set = array($size_set);
        }

        if (in_array(self::SIZE_SET_ALL, $size_set)) {
            $sizes = self::$sizes_origin + self::$sizes_squared
             + self::$sizes_portrait + self::$sizes_wide;
        } else {
            $sizes = self::$sizes_origin;
            if (in_array(self::SIZE_SET_SQUARED, $size_set)) {
                $sizes += self::$sizes_squared;
            }

            if (in_array(self::SIZE_SET_PORTRAIT, $size_set)) {
                $sizes += self::$sizes_portrait;
            }

            if (in_array(self::SIZE_SET_WIDE, $size_set)) {
                $sizes += self::$sizes_wide;
            }

            if (in_array(self::SIZE_SET_RATIO, $size_set)) {
                $sizes += self::$sizes_blog;
            }

        }
        return $sizes;
    }

    protected static $sizes_squared = array(
        Image::SIZE_SQUARED_XS => array(
            "width" => 40,
            "height" => 40,
        ),
        Image::SIZE_SQUARED_S => array(
            "width" => 50,
            "height" => 50,
        ),
        Image::SIZE_SQUARED_M => array(
            "width" => 100,
            "height" => 100,
        ),
        Image::SIZE_SQUARED_L => array(
            "width" => 200,
            "height" => 200,
        ),
        Image::SIZE_SQUARED_XL => array(
            "width" => 600,
            "height" => 600,
        ),
    );

    protected static $sizes_portrait = array(
        Image::SIZE_PORT_XS => array(
            "width" => 90,
            "height" => 120,
        ),
        Image::SIZE_PORT_S => array(
            "width" => 120,
            "height" => 160,
        ),
        Image::SIZE_PORT_M => array(
            "width" => 180,
            "height" => 240,
        ),
        Image::SIZE_PORT_L => array(
            "width" => 240,
            "height" => 320,
        ),
        Image::SIZE_PORT_XL => array(
            "width" => 320,
            "height" => 426,
        ),
    );

    protected static $sizes_wide = array(
        Image::SIZE_WIDE_S => array(
            "height" => 90,
        ),
        Image::SIZE_WIDE_M => array(
            "width" => 600,
        ),
        Image::SIZE_WIDE_L => array(
            "width" => 750,
        ),
    );

    protected static $sizes_blog = array(

        Image::SIZE_BLOG_XL => array(
            "width" => 640,
            "height" => 360,
        ),

        Image::SIZE_BLOG_L => array(
            "width" => 322,
            "height" => 182,
        ),
        Image::SIZE_BLOG_M => array(
            "width" => 322,
            "height" => 177,
        ),
        Image::SIZE_BLOG_S => array(
            "width" => 215,
            "height" => 119,
        ),
        Image::SIZE_ORIGINAL => array(),
    );

    public static $sizes_origin = array(
        Image::SIZE_ORIGINAL => array(),
    );
}
