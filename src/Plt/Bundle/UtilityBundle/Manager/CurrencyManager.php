<?php

namespace Plt\Bundle\UtilityBundle\Manager;

use Plt\Bundle\UtilityBundle\Entity\Currency;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class CurrencyManager extends EntityManager
{

    public function getCurrenciesQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('c');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getCurrencies($searchQuery, $limit = 10, $offset = 0)
    {
        $currencyQueryBuilder = $this->getCurrenciesQueryBuilder($limit, $offset);

        if (null != $searchQuery) {
            $currencyQueryBuilder->where(
                $currencyQueryBuilder->expr()->like('c.name', $currencyQueryBuilder->expr()->literal(sprintf('%%%s%%', $searchQuery)))
            );
        }

        return $currencyQueryBuilder->getQuery()->execute();
    }

    public function addCurrency($mixed)
    {
        $currency = new Currency();

        ArrayUtil::toEntity($currency, $mixed);

        $errors = $this->validate($currency);
        if (null != $errors) {
            return $errors;
        }

        $this->save($currency);

        return $currency;
    }

    public function search($text)
    {

    }
}
