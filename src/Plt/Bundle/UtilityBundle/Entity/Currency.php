<?php

namespace Plt\Bundle\UtilityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Currency
 *
 * @ORM\Table(name="Currency", indexes={
 *      @ORM\Index(name="currency_slug", columns={"slug"}),
 *      @ORM\Index(name="currency_currency_symbol", columns={"currency_symbol"}),
 *      @ORM\Index(name="currency_currency_code", columns={"currency_code"}),
 * })
 * @ORM\Entity
 */
class Currency
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=32)
     * @Assert\NotBlank()
     *
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", length=32)
     * @Assert\NotBlank()
     *
     */
    private $slug;

    /**
     * @ORM\Column(name="currency_code", type="string", length=4)
     * @Assert\NotBlank()
     *
     */
    private $currencyCode;

    /**
     * @ORM\Column(name="currency_symbol", type="string", length=4, nullable=true)
     */
    private $currencySymbol;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Currency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Currency
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     * @return Currency
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Set currencySymbol
     *
     * @param string $currencySymbol
     * @return Currency
     */
    public function setCurrencySymbol($currencySymbol)
    {
        $this->currencySymbol = $currencySymbol;

        return $this;
    }

    /**
     * Get currencySymbol
     *
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->currencySymbol;
    }
}
