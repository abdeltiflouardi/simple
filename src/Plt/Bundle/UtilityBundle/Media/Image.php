<?php

namespace Plt\Bundle\UtilityBundle\Media;

use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * This class represents an image stored in our cdn, and also contains a set of
 * helpers allowing to deal with stored images.
 */
class Image
{
    const SIZE_SQUARED_XS = "s-xs";
    const SIZE_SQUARED_S = "s-s";
    const SIZE_SQUARED_M = "s-m";
    const SIZE_SQUARED_L = "s-l";
    const SIZE_SQUARED_XL = "s-xl";

    const SIZE_PORT_XS = "p-xs";
    const SIZE_PORT_S = "p-s";
    const SIZE_PORT_M = "p-m";
    const SIZE_PORT_L = "p-l";
    const SIZE_PORT_XL = "p-xl";

    const SIZE_WIDE_S = "w-s";
    const SIZE_WIDE_M = "w-m";
    const SIZE_WIDE_L = "w-l";

    const SIZE_BLOG_S = "b-s";
    const SIZE_BLOG_M = "b-m";
    const SIZE_BLOG_L = "b-l";
    const SIZE_BLOG_XL = "b-xl";

    const SIZE_ORIGINAL = "orig";

    const DEFAULT_IMAGE_KEY = '0000000000000000000000000000000000000000';

    const DEFAULT_SIZE = self::SIZE_PORT_L;

    public static $all_sizes = array(
        self::SIZE_SQUARED_XS, self::SIZE_SQUARED_S, self::SIZE_SQUARED_M, self::SIZE_SQUARED_L,
        self::SIZE_PORT_XS, self::SIZE_PORT_S, self::SIZE_PORT_M, self::SIZE_PORT_L,
        self::SIZE_WIDE_S, self::SIZE_WIDE_S, self::SIZE_WIDE_S,
        self::SIZE_ORIGINAL,
    );

    public function __construct($key, $root_path = "")
    {
        $this->key = $key;
        $this->root_path = $root_path;
    }

    public function getUrl($size = self::DEFAULT_SIZE)
    {
        return $this->root_path . self::generateSuffix($this->key, $size);
    }

    public static function generateSuffix($key, $size, $extension = ".jpg")
    {
        return '/' . substr($key, 0, 2) . '/' . $key . '.' . $size . $extension;
    }

    /**
     * Returns the path of the image identified by `$key`, relative to
     * `$storage_path`.
     *
     * @param string $key
     * @param string $size
     * @param string $storage_path
     *
     * @return string
     */
    public static function generateStorageLocation($key, $size, $storage_path)
    {
        return $storage_path . self::generateSuffix($key, $size);
    }

    public static function getDefaultImage($media_path)
    {
        return new self(self::DEFAULT_IMAGE_KEY, $media_path);
    }

    /**
     * Delete the image identified by $key, in all stored sizes.
     *
     * @param string $key
     * @param string $storage_path
     *
     * @return bool returns false in case of error
     */
    public static function deleteImage($key, $storage_path)
    {
        $fs = new Filesystem();
        $finder = new Finder();

        try {
            $finder->in($storage_path)->files()->name("$key*");
            $fs->remove($finder);
        } catch (IOException $e) {
            return false;
        }

        return true;
    }
}
