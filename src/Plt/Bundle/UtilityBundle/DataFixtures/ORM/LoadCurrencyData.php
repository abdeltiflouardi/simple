<?php

namespace TBM\Bundle\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Plt\Bundle\UtilityBundle\Entity\Currency;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

class LoadCurrencyData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $data = Yaml::parse(file_get_contents(__DIR__ . '/../../Resources/data/currencies.yml'));

        $currencies = array();
        foreach ($data as $currencyData) {
            $currency = new Currency();

            $currency
                ->setName($currencyData['currency_name'])
                ->setSlug($currencyData['currency_name'])
                ->setCurrencyCode($currencyData['currency_code'])
                ->setCurrencySymbol($currencyData['currency_symbol'])
            ;

            $manager->persist($currency);

            $this->setReference('currencies' . $currencyData['currency_code'], $currency);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
