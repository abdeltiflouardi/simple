<?php

namespace Plt\Bundle\UtilityBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**
     * return \Plt\Bundle\UtilityBundle\Manager\CurrecyManager
     */
    public function getCurrencyManager()
    {
        return $this->container->get('plt_currency.manager');
    }

    /**
     * List all currencies.
     *
     * @ApiDoc(
     *     section="/currencies",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="search_query", description="Word to search.")
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list currencies.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many currencies to return.")
     *
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getCurrenciesAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;
        $searchQuery = $paramFetcher->get('search_query');

        return $this->getCurrencyManager()->getCurrencies($searchQuery, $limit, $offset);
    }

    /**
     * Create a new currency.
     *
     * @ApiDoc(
     *   section="/currencies",
     *   statusCodes={
     *     200="Returned when successful",
     *     400="Returned when the form has errors"
     *   }
     * )
     *
     * @return RouteRedirectView
     */
    public function postCurrenciesAction(Request $request)
    {
        $data = $request->get('data');

        $currency = $this->getCurrencyManager()->addCurrency($data);

        return $currency;
    }
}
