<?php

namespace Plt\Bundle\EntityBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Plt\Bundle\ApiBundle\Controller\BaseApiController as Controller;
use Plt\Bundle\EntityBundle\Entity\Entity;
use Plt\Bundle\EntityBundle\Entity\EntityCalendar;
use Plt\Bundle\EntityBundle\Entity\EntityCollection;
use Plt\Bundle\LocationBundle\Entity\City;
use Plt\Bundle\LocationBundle\Entity\Country;
use Plt\Bundle\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**********************
     *                    *
     *     API Entity     *
     *                    *
     **********************/

    /**
     * @return \Plt\Bundle\EntityBundle\Manager\EntityManager;
     */
    public function getEntityManager()
    {
        return $this->container->get('plt_entity.manager');
    }

    /**
     * List all entities.
     *
     * @ApiDoc(
     *     section="/entities",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of entities.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many entities to return.")
     *
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return \Plt\Bundle\UserBundle\Entity\User[]
     */
    public function getEntitiesAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getEntityManager()->getEntities($limit, $offset);
    }

    /**
     * Get entities by Country.
     *
     * @ApiDoc(
     *     section="/entities",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     },
     *     requirements={
     *         {"name"="country_id", "dataType"="integer", "requirement"="\d+", "description"="country id"}
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list entities.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many entities to return.")
     *
     * @Annotations\Get("/entities/countries/{country_id}")
     * @Annotations\View()
     *
     * @ParamConverter("country", class="PltLocationBundle:Country", options={"id": "country_id"})
     *
     * @param Country               $country        the country object
     * @param ParamFetcherInterface $paramFetcher   param fetcher service
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address[]
     */
    public function getEntitiesCountriesAction(Country $country, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getEntityManager()->getEntitiesByCountry($country, $limit, $offset);
    }

    /**
     * Get entities by City.
     *
     * @ApiDoc(
     *     section="/entities",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     },
     *     requirements={
     *         {"name"="city_id", "dataType"="integer", "requirement"="\d+", "description"="city id"}
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list entities.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many entities to return.")
     *
     * @Annotations\Get("/entities/cities/{city_id}")
     * @Annotations\View()
     *
     * @ParamConverter("city", class="PltLocationBundle:City", options={"id": "city_id"})
     *
     * @param City                  $city           the city object
     * @param ParamFetcherInterface $paramFetcher   param fetcher service
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address[]
     */
    public function getEntitiesCitiesAction(City $city, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getEntityManager()->getEntitiesByCity($city, $limit, $offset);
    }

    /**
     * Get entities by User.
     *
     * @ApiDoc(
     *     section="/entities",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     },
     *     requirements={
     *         {"name"="user_id", "dataType"="integer", "requirement"="\d+", "description"="user id"}
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list entities.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many entities to return.")
     *
     * @Annotations\Get("/entities/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     *
     * @param User                  $user           the user object
     * @param ParamFetcherInterface $paramFetcher   param fetcher service
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address[]
     */
    public function getEntitiesUsersAction(User $user, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getEntityManager()->getEntitiesByUser($user, $limit, $offset);
    }

    /**
     * Create new entity
     *
     * @ApiDoc(
     *      section="/entities",
     * )
     *
     * @Annotations\Post("/entities/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function postEntitiesAction(User $user, Request $request)
    {
        $data = $request->get('data');

        $entity = $this->getEntityManager()->createEntity($user, $data);

        return $entity;
    }

    /**
     * Update entity
     *
     * @ApiDoc(
     *      section="/entities",
     * )
     *
     * @Annotations\Put("/entities/{entity_id}/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function putEntitiesAction(Entity $entity, User $user, Request $request)
    {
        $data = $request->get('data');

        $entity = $this->getEntityManager()->updateEntity($user, $entity, $data);

        return $entity;
    }

    /**
     * Toggle live entity
     *
     * @ApiDoc(
     *      section="/entities",
     * )
     *
     * @Annotations\Post("/entities/{entity_id}/users/{user_id}/toggle-is-live")
     * @Annotations\View()
     *
     * @ParamConverter("entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function postEntitiesToggleLiveAction(Entity $entity, User $user)
    {
        $entity = $this->getEntityManager()->toggleEntityLive($user, $entity);

        return $entity;
    }

    /**
     * Delete entity
     *
     * @ApiDoc(
     *      section="/entities",
     * )
     *
     * @Annotations\Delete("/entities/{entity_id}/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function deleteEntitiesAction(Entity $entity, User $user)
    {
        $this->getEntityManager()->deleteEntity($user, $entity);
    }

    /*********************************
     *                               *
     *     API EntityCollection      *
     *                               *
     *********************************/

    /**
     * @return \Plt\Bundle\EntityBundle\Manager\EntityCollectionManager
     */
    public function getEntityCollectionManager()
    {
        return $this->container->get('plt_entity_collection.manager');
    }

    /**
     * Create new entity collection
     *
     * @ApiDoc(
     *      section="/entities",
     * )
     *
     * @Annotations\Post("/entities/collections/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter(name="user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function postEntityCollectionAction(User $user, Request $request)
    {
        $data = $request->get('data');

        $collection = $this->getEntityCollectionManager()->createEntityCollection($user, $data);

        return $collection;
    }

    /**
     * Add entity to collection
     *
     * @ApiDoc(
     *      section="/entities",
     * )
     *
     * @Annotations\Post("/entities/collections/{collection_id}/entities/{entity_id}/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter(name="entityCollection", class="PltEntityBundle:EntityCollection", options={"id": "collection_id"})
     * @ParamConverter(name="entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     * @ParamConverter(name="user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function postEntityToCollectionAction(User $user, Entity $entity, EntityCollection $entityCollection)
    {
        $line = $this->getEntityCollectionManager()->addEntity($user, $entity, $entityCollection);

        return $line;
    }

    /**
     * Delete entity to collection
     *
     * @ApiDoc(
     *      section="/entities",
     * )
     *
     * @Annotations\Delete("/entities/collections/{collection_id}/entities/{entity_id}/users/{user_id}")
     *
     * @ParamConverter(name="entityCollection", class="PltEntityBundle:EntityCollection", options={"id": "collection_id"})
     * @ParamConverter(name="entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     * @ParamConverter(name="user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function deleteEntityToCollectionEntityAction(User $user, Entity $entity, EntityCollection $entityCollection)
    {
        $this->getEntityCollectionManager()->deleteEntityFromEntityCollection($user, $entity, $entityCollection);
    }

    /**
     * Toggle live entity collection
     *
     * @ApiDoc(
     *      section="/entities",
     * )
     *
     * @Annotations\Post("/entities/collections/{collection_id}/users/{user_id}/toggle-is-live")
     *
     * @ParamConverter(name="entityCollection", class="PltEntityBundle:EntityCollection", options={"id": "collection_id"})
     * @ParamConverter(name="user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function postCollectionToggleLiveAction(User $user, EntityCollection $entityCollection)
    {
        $collection = $this->getEntityCollectionManager()->toggleEntityCollectionLive($user, $entityCollection);

        return $collection;
    }

    /**
     * Get entities by collection
     *
     * @ApiDoc(
     *      section="/entities",
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list entities.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many entities to return.")
     *
     * @Annotations\Get("/entities/collections/{collection_id}/users/{user_id}")
     *
     * @ParamConverter(name="entityCollection", class="PltEntityBundle:EntityCollection", options={"id": "collection_id"})
     * @ParamConverter(name="user", class="PltUserBundle:User", options={"id": "user_id"})
     */
    public function getEntityCollectionAction(User $user, EntityCollection $entityCollection, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getEntityManager()->getEntitiesByCollection($entityCollection, $limit, $offset);
    }

    /********************************
     *                              *
     *      API EntityCalendar      *
     *                              *
     ********************************/

    public function getEntityCalendarManager()
    {
        return $this->container->get('plt_entity_calendar.manager');
    }

    /**
     * @ApiDoc(
     *      section="/entities"
     * )
     *
     * @Annotations\Post("/entities/calendar/{entity_id}")
     *
     * @ParamConverter(name="entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     */
    public function postEntityCalendarAction(Entity $entity, Request $request)
    {
        $data = $request->get('data');

        $calendar = $this->getEntityCalendarManager()->addEntityCalendar($entity, $data);

        return $calendar;
    }

    /**
     * @ApiDoc(
     *      section="/entities"
     * )
     *
     * @Annotations\Put("/entities/calendar/{entity_calendar_id}")
     *
     * @ParamConverter(name="entity", class="PltEntityBundle:EntityCalendar", options={"id": "entity_calendar_id"})
     */
    public function putEntityCalendarAction(EntityCalendar $entityCalendar, Request $request)
    {
        $user = $this->getUser();
        $data = $request->get('data');

        $calendar = $this->getEntityCalendarManager()->updateEntityCalendar($user, $entityCalendar, $data);

        return $calendar;
    }

    /**
     * @ApiDoc(
     *      section="/entities"
     * )
     *
     * @Annotations\QueryParam(name="month", requirements="\d+", default=1, description="Months.")
     *
     * @Annotations\Get("/entities/calendar/{entity_id}")
     *
     * @ParamConverter(name="entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     */
    public function getEntityCalendarEntityAction(Entity $entity, ParamFetcherInterface $paramFetcher)
    {
        $nbMonth = $paramFetcher->get('month');

        return $this->getEntityCalendarManager()->getEntityCalendarByEntity($entity, $nbMonth);
    }
}
