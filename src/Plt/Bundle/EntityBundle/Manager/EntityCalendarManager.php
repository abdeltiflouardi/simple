<?php

namespace Plt\Bundle\EntityBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as BaseEntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class EntityCalendarManager extends BaseEntityManager
{
    public function getQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('e');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getEntityCalendarByEntity($entity, $limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('ec');

        $qb
            ->where('ec.entity = :entity')
            ->setParameter('entity', VarUtil::toInt($entity))
        ;

        return $qb->getQuery()->execute();
    }

    public function addEntityCalendar($entity, $data)
    {
        $data['entity'] = $entity;

        $calendar = $this->createInstance();

        ArrayUtil::toEntity($calendar, $data);

        $errors = $this->validate($calendar);
        if (null != $errors) {
            return $errors;
        }

        $this->save($calendar);

        return $calendar;
    }

    public function updateEntityCalendar($user, $calendar, $data)
    {
        ArrayUtil::toEntity($calendar, $data);

        $errors = $this->validate($calendar);
        if (null != $errors) {
            return $errors;
        }

        $this->save($calendar);

        return $calendar;
    }
}
