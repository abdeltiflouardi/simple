<?php

namespace Plt\Bundle\EntityBundle\Manager;

use Plt\Bundle\EntityBundle\Entity\Entity;
use Plt\Bundle\EntityBundle\Entity\EntityCollection;
use Plt\Bundle\EntityBundle\Entity\EntityCollectionLine;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class EntityCollectionManager extends EntityManager
{
    public function getEntitiesCollectionQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('ec');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getEntitiesCollection($limit, $offset)
    {
        $ecQueryBuilder = $this->getEntitiesCollectionQueryBuilder($limit, $offset);

        return $ecQueryBuilder->getQuery()->execute();
    }

    public function getEntityCollection($user, $entityCollection, $limit = 10, $offset = 0)
    {
        $ecQueryBuilder = $this->getEntitiesCollectionQueryBuilder($limit, $offset);

        $entityQueryBuilder
            ->where('ec.createdBy = :user')
            ->setParameter('user', VarUtil::toInt($user))
        ;

        return $ecQueryBuilder->getQuery()->execute();
    }

    public function addEntity($user, $entity, $entityCollection)
    {
        return $this->createEntityCollectionLine($entity, $entityCollection);
    }

    public function toggleEntityCollectionLive($user, $entityCollection)
    {
        $entityCollection->setIsLive(!$entityCollection->getIsLive());

        $this->save($entityCollection);

        return $entityCollection;
    }

    public function deleteEntityFromEntityCollection($user, $entity, $entityCollection)
    {
        $this->deleteEntityCollectionLine($entity, $entityCollection);
    }

    public function createEntityCollection($user, $data)
    {
        $data['createdBy'] = $user;

        $entity = $this->createInstance();
        ArrayUtil::toEntity($entity, $data);

        $errors = $this->validate($entity);
        if (null != $errors) {
            return $errors;
        }

        $this->save($entity);

        return $entity;
    }

    public function getCollectionLine(Entity $entity, EntityCollection $entityCollection)
    {
        $lineRepo = $this->em->getRepository('PltEntityBundle:EntityCollectionLine');

        $qb = $lineRepo->createQueryBuilder('l');

        $qb
            ->where('l.entity = :entity')
            ->setParameter('entity', VarUtil::toInt($entity))

            ->andWhere('l.entityCollection = :entityCollection')
            ->setParameter('entityCollection', VarUtil::toInt($entityCollection))

            ->setFirstResult(0)
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function createEntityCollectionLine(Entity $entity, EntityCollection $entityCollection)
    {
        $line = $this->getCollectionLine($entity, $entityCollection);
        if ($line) {
            return $line;
        }

        $line = new EntityCollectionLine();
        $line->setEntity($entity);
        $line->setEntityCollection($entityCollection);

        $this->save($line);

        return $line;
    }

    public function deleteEntityCollectionLine(Entity $entity, EntityCollection $entityCollection)
    {
        $line = $this->getCollectionLine($entity, $entityCollection);
        if ($line) {
            $this->delete($line);
        }
    }
}
