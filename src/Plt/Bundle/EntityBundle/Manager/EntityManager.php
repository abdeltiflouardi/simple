<?php

namespace Plt\Bundle\EntityBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as BaseEntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class EntityManager extends BaseEntityManager
{
    public function getEntitiesQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('e');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getEntitiesByCollection($collection, $limit = 10, $offset = 0)
    {
        $qb = $this->getEntitiesQueryBuilder($limit, $offset);

        $qb
            ->join('e.entityCollectionLines', 'ecl')

            ->where('ecl.entityCollection = :ec')
            ->setParameter('ec', VarUtil::toInt($collection))
        ;

        return $qb->getQuery()->execute();
    }

    public function getEntities($limit, $offset)
    {
        $entityQueryBuilder = $this->getEntitiesQueryBuilder($limit, $offset);

        return $entityQueryBuilder->getQuery()->execute();
    }

    public function getEntitiesByCity($city, $limit, $offset)
    {
        $entityQueryBuilder = $this->getEntitiesQueryBuilder($limit, $offset);

        $entityQueryBuilder
            ->where('e.city = :city')
            ->setParameter('city', VarUtil::toInt($city))
        ;

        return $entityQueryBuilder->getQuery()->execute();
    }

    public function getEntitiesByCountry($country, $limit, $offset)
    {
        $entityQueryBuilder = $this->getEntitiesQueryBuilder($limit, $offset);

        $entityQueryBuilder
            ->where('e.country = :country')
            ->setParameter('country', VarUtil::toInt($country))
        ;

        return $entityQueryBuilder->getQuery()->execute();
    }

    public function getEntitiesByUser($user, $limit, $offset)
    {
        $entityQueryBuilder = $this->getEntitiesQueryBuilder($limit, $offset);

        $entityQueryBuilder
            ->where('e.user = :user')
            ->setParameter('user', VarUtil::toInt($user))
        ;

        return $entityQueryBuilder->getQuery()->execute();
    }

    public function createEntity($user, $data)
    {
        $data['user'] = $user;

        $entity = $this->createInstance();

        return $this->updateEntity($user, $entity, $data);
    }

    public function deleteEntity($user, $entity)
    {
        $this->delete($entity);
    }

    public function updateEntity($user, $entity, $data)
    {
        ArrayUtil::toEntity($entity, $data);

        $errors = $this->validate($entity);
        if (null != $errors) {
            return $errors;
        }

        $this->save($entity);

        return $entity;
    }

    public function toggleEntityLive($user, $entity)
    {
        $entity->setIsLive(!$entity->getIsLive());

        $this->save($entity);

        return $entity;
    }
}
