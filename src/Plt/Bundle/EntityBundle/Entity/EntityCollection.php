<?php

namespace Plt\Bundle\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EntityCollection
 *
 * @ORM\Table(name="EntityCollection", indexes={
 *      @ORM\Index(name="entity_collection_is_live", columns={"is_live"}),
 *      @ORM\Index(name="entity_collection_slug", columns={"slug"})
 * })
 * @ORM\Entity
 */
class EntityCollection
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=64)
     *
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", length=64)
     *
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @ORM\Column(name="description", type="string", length=255)
     *
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @ORM\Column(name="is_live", type="boolean")
     */
    private $isLive;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     */
    private $createdBy;

    public function __construct()
    {
        $this->isLive = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EntityCollection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return EntityCollection
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return EntityCollection
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isLive
     *
     * @param boolean $isLive
     * @return EntityCollection
     */
    public function setIsLive($isLive)
    {
        $this->isLive = $isLive;

        return $this;
    }

    /**
     * Get isLive
     *
     * @return boolean
     */
    public function getIsLive()
    {
        return $this->isLive;
    }

    /**
     * Set createdBy
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $createdBy
     * @return EntityCollection
     */
    public function setCreatedBy(\Plt\Bundle\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
