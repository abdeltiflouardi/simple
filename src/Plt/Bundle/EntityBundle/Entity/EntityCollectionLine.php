<?php

namespace Plt\Bundle\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntityCollectionLine
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class EntityCollectionLine
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EntityBundle\Entity\EntityCollection")
     */
    private $entityCollection;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EntityBundle\Entity\Entity")
     */
    private $entity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entityCollection
     *
     * @param \Plt\Bundle\EntityBundle\Entity\EntityCollection $entityCollection
     * @return EntityCollectionLine
     */
    public function setEntityCollection(\Plt\Bundle\EntityBundle\Entity\EntityCollection $entityCollection = null)
    {
        $this->entityCollection = $entityCollection;

        return $this;
    }

    /**
     * Get entityCollection
     *
     * @return \Plt\Bundle\EntityBundle\Entity\EntityCollection 
     */
    public function getEntityCollection()
    {
        return $this->entityCollection;
    }

    /**
     * Set entity
     *
     * @param \Plt\Bundle\EntityBundle\Entity\Entity $entity
     * @return EntityCollectionLine
     */
    public function setEntity(\Plt\Bundle\EntityBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Plt\Bundle\EntityBundle\Entity\Entity 
     */
    public function getEntity()
    {
        return $this->entity;
    }
}
