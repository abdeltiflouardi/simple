<?php

namespace Plt\Bundle\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity
 *
 * @ORM\Table(name="Entity", indexes={
 *      @ORM\Index(name="entity_is_live", columns={"is_live"}),
 *      @ORM\Index(name="entity_lat_lon", columns={"lat", "lon"}),
 *      @ORM\Index(name="entity_review_score", columns={"review_score"}),
 * })
 *
 * @ORM\Entity
 */
class Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="lat", type="decimal", scale=12, precision=18, nullable=true)
     */
    private $lat;

    /**
     * @ORM\Column(name="lon", type="decimal", scale=12, precision=18, nullable=true)
     */
    private $lon;

    /**
     * @ORM\Column(name="name", type="string", length=64, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="long_description", type="text", nullable=true)
     */
    private $longDescription;

    /**
     * @ORM\Column(name="short_description", type="string", length=255, nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\Column(name="rules", type="text", nullable=true)
     */
    private $rules;

    /**
     * @ORM\Column(name="minimum_booking", type="integer", nullable=true)
     */
    private $minimumBooking;

    /**
     * @ORM\Column(name="floor_price", type="decimal", scale=2)
     *
     * @Assert\NotBlank()
     */
    private $floorPrice;

    /**
     * @ORM\Column(name="deposit", type="decimal", scale=2)
     *
     * @Assert\NotBlank()
     */
    private $deposit;

    /**
     * @ORM\Column(name="review_score", type="decimal", scale=2)
     *
     * @Assert\NotBlank()
     */
    private $reviewScore;

    /**
     * @ORM\Column(name="is_live", type="boolean")
     *
     */
    private $isLive;

    /**
     * @ORM\ManyToOne(targetEntity="Plt\Bundle\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Plt\Bundle\LocationBundle\Entity\City")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="Plt\Bundle\LocationBundle\Entity\Country")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="Plt\Bundle\TransactionBundle\Entity\CancellationPolicy")
     * @ORM\JoinColumn(name="cancellation_policy_id", referencedColumnName="id")
     */
    private $cancellationPolicy;

    /**
     * @ORM\OneToMany(targetEntity="Plt\Bundle\EntityBundle\Entity\EntityCollectionLine", mappedBy="entity")
     */
    private $entityCollectionLines;

    public function __construct()
    {
        $this->reviewScore = 0;
        $this->isLive = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return Entity
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return Entity
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return string
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Entity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set longDescription
     *
     * @param string $longDescription
     * @return Entity
     */
    public function setLongDescription($longDescription)
    {
        $this->longDescription = $longDescription;

        return $this;
    }

    /**
     * Get longDescription
     *
     * @return string
     */
    public function getLongDescription()
    {
        return $this->longDescription;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return Entity
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set rules
     *
     * @param string $rules
     * @return Entity
     */
    public function setRules($rules)
    {
        $this->rules = $rules;

        return $this;
    }

    /**
     * Get rules
     *
     * @return string
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Set minimumBooking
     *
     * @param integer $minimumBooking
     * @return Entity
     */
    public function setMinimumBooking($minimumBooking)
    {
        $this->minimumBooking = $minimumBooking;

        return $this;
    }

    /**
     * Get minimumBooking
     *
     * @return integer
     */
    public function getMinimumBooking()
    {
        return $this->minimumBooking;
    }

    /**
     * Set floorPrice
     *
     * @param string $floorPrice
     * @return Entity
     */
    public function setFloorPrice($floorPrice)
    {
        $this->floorPrice = $floorPrice;

        return $this;
    }

    /**
     * Get floorPrice
     *
     * @return string
     */
    public function getFloorPrice()
    {
        return $this->floorPrice;
    }

    /**
     * Set deposit
     *
     * @param string $deposit
     * @return Entity
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Get deposit
     *
     * @return string
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set reviewScore
     *
     * @param string $reviewScore
     * @return Entity
     */
    public function setReviewScore($reviewScore)
    {
        $this->reviewScore = $reviewScore;

        return $this;
    }

    /**
     * Get reviewScore
     *
     * @return string
     */
    public function getReviewScore()
    {
        return $this->reviewScore;
    }

    /**
     * Set isLive
     *
     * @param boolean $isLive
     * @return Entity
     */
    public function setIsLive($isLive)
    {
        $this->isLive = $isLive;

        return $this;
    }

    /**
     * Get isLive
     *
     * @return boolean
     */
    public function getIsLive()
    {
        return $this->isLive;
    }

    /**
     * Set user
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $user
     * @return Entity
     */
    public function setUser(\Plt\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set city
     *
     * @param \Plt\Bundle\LocationBundle\Entity\City $city
     * @return Entity
     */
    public function setCity(\Plt\Bundle\LocationBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Plt\Bundle\LocationBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param \Plt\Bundle\LocationBundle\Entity\Country $country
     * @return Entity
     */
    public function setCountry(\Plt\Bundle\LocationBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Plt\Bundle\LocationBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set cancellationPolicy
     *
     * @param \Plt\Bundle\TransactionBundle\Entity\CancellationPolicy $cancellationPolicy
     * @return Entity
     */
    public function setCancellationPolicy(\Plt\Bundle\TransactionBundle\Entity\CancellationPolicy $cancellationPolicy = null)
    {
        $this->cancellationPolicy = $cancellationPolicy;

        return $this;
    }

    /**
     * Get cancellationPolicy
     *
     * @return \Plt\Bundle\TransactionBundle\Entity\CancellationPolicy
     */
    public function getCancellationPolicy()
    {
        return $this->cancellationPolicy;
    }
}
