<?php

namespace Plt\Bundle\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntityStatus
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class EntityStatus
{
    const BOOKED = 'BOOKED';

    // Duration dynamique
    const ON_HOLD = 'ON_HOLD';

    // Avec un prix différent
    const AVAILABLE = 'AVAILABLE';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="statusName", type="string", length=16)
     */
    private $statusName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isAvailable", type="boolean")
     */
    private $isAvailable;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusName
     *
     * @param string $statusName
     * @return EntityStatus
     */
    public function setStatusName($statusName)
    {
        $this->statusName = $statusName;

        return $this;
    }

    /**
     * Get statusName
     *
     * @return string
     */
    public function getStatusName()
    {
        return $this->statusName;
    }

    /**
     * Set isAvailable
     *
     * @param boolean $isAvailable
     * @return EntityStatus
     */
    public function setIsAvailable($isAvailable)
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    /**
     * Get isAvailable
     *
     * @return boolean
     */
    public function getIsAvailable()
    {
        return $this->isAvailable;
    }
}
