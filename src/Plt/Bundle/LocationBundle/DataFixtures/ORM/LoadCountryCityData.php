<?php

namespace TBM\Bundle\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Plt\Bundle\LocationBundle\Entity\Country;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

class LoadCountryCityData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $data = Yaml::parse(file_get_contents(__DIR__ . '/../../Resources/data/countries.yml'));

        $country = array();
        foreach ($data as $countryData) {
            $countryCode = strtolower($countryData['countryCode']);
            $country[$countryCode] = new Country();

            $country[$countryCode]
                ->setCountryCode($countryData['countryCode'])
                ->setName($countryData['countryName'])
                ->setSlug($countryData['countryName'])
                ->setIsoNumeric($countryData['isoNumeric'])
            ;

            if ($this->hasReference('currencies' . $countryData['currencyCode'])) {
                $country[$countryCode]->setCurrency($this->getReference('currencies' . $countryData['currencyCode']));
            }

            $manager->persist($country[$countryCode]);
        }

        $manager->flush();

        $this->setReference('country', $country['fr']);

        $this->loadCity($manager, $country);
    }

    public function loadCity($manager, $country)
    {
        $finder = new Finder();
        $finder->name("*tv*")->in(__DIR__ . '/../../Resources/data/cities/');

        foreach ($finder as $file) {
            echo $file . "\n";
            $data = Yaml::parse(file_get_contents($file));

            $manager->getConnection()->beginTransaction();
            foreach ($data as $cityData) {
                extract($cityData); // $countryCode, $slug, $name, $region, $population, $lat, $lon

                if (!isset($country[$countryCode])) {
                    continue;
                }

                $countryId = $country[$countryCode]->getId();

                $cityInsertSQL = 'INSERT INTO City (name, slug, region, lat, lon, country_id) VALUES (?, ?, ?, ?, ?, ?)';

                $manager->getConnection()->executeUpdate($cityInsertSQL, array($name, $slug, $region, $lat, $lon, $countryId));
            }
            $manager->getConnection()->commit();
        }
    }

    public function getOrder()
    {
        return 2;
    }
}
