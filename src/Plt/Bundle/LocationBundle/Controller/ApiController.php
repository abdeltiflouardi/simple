<?php

namespace Plt\Bundle\LocationBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Plt\Bundle\ApiBundle\Controller\BaseApiController as Controller;
use Plt\Bundle\LocationBundle\Entity\Country;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{

    /**
     * return \Plt\Bundle\LocationBundle\Manager\CountryManager
     */
    public function getCountryManager()
    {
        return $this->container->get('plt_country.manager');
    }

    /**
     * return \Plt\Bundle\LocationBundle\Manager\CityManager
     */
    public function getCityManager()
    {
        return $this->container->get('plt_city.manager');
    }

    /**
     * List all countries.
     *
     * @ApiDoc(
     *     section="/countries",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="search_query", description="Word to search.")
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list countries.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many countries to return.")
     *
     * @Annotations\View()
     *
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getCountriesAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;
        $searchQuery = $paramFetcher->get('search_query');

        return $this->getCountryManager()->getCountries($searchQuery, $limit, $offset);
    }

    /**
     * List all cities by country.
     *
     * @ApiDoc(
     *     section="/cities",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list cities.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many cities to return.")
     *
     * @Annotations\Get("/cities/countries/{country_id}")
     * @Annotations\View()
     *
     * @ParamConverter("country", class="PltLocationBundle:Country", options={"id": "country_id"})
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getCountriesCitiesAction(Country $country, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getCityManager()->getCitiesByCountry($country, $limit, $offset);
    }

    /**
     * Create a new country.
     *
     * @ApiDoc(
     *   section="/countries",
     *   statusCodes={
     *     200="Returned when successful",
     *     400="Returned when the form has errors"
     *   }
     * )
     *
     * @return RouteRedirectView
     */
    public function postCountriesAction(Request $request)
    {
        $data = $request->get('data');

        $country = $this->getCountryManager()->addCountry($data);

        return $country;
    }

    /**
     * Search cities.
     *
     * @ApiDoc(
     *     section="/cities",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *     }
     * )
     *
     * @Annotations\QueryParam(name="search_query", description="Word to search.")
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list cities.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many cities to return.")
     *
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getCitiesAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;
        $searchQuery = $paramFetcher->get('search_query');

        return $this->getCityManager()->getCities($searchQuery, $limit, $offset);
    }

    /**
     * Create a new city in given country.
     *
     * @ApiDoc(
     *   section="/cities",
     *   statusCodes={
     *     200="Returned when successful",
     *     400="Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\Post("/cities/countries/{country_id}")
     *
     * @ParamConverter("country", class="PltLocationBundle:Country", options={"id": "country_id"})
     *
     * @return RouteRedirectView
     */
    public function postCitiesCountriesAction(Request $request, Country $country)
    {
        $data = $request->get('data');

        $city = $this->getCityManager()->addCity($data, $country);

        return $city;
    }
}
