<?php

namespace Plt\Bundle\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * City
 *
 * @ORM\Table(name="City", indexes={
 *      @ORM\Index(name="city_slug", columns={"slug"}),
 *      @ORM\Index(name="city_name", columns={"name"}),
 *      @ORM\Index(name="city_lat_lon", columns={"lat", "lon"}),
 * })
 * @ORM\Entity
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=64)
     * @Assert\NotBlank()
     *
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", length=64)
     * @Assert\NotBlank()
     *
     */
    private $slug;

    /**
     * @ORM\Column(name="region", type="string", length=64)
     * @Assert\NotBlank()
     *
     */
    private $region;

    /**
     * @ORM\Column(name="lat", type="string", length=64)
     * @Assert\NotBlank()
     *
     */
    private $lat;

    /**
     * @ORM\Column(name="lon", type="string", length=64)
     * @Assert\NotBlank()
     *
     */
    private $lon;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\LocationBundle\Entity\Country")
     * @Assert\NotBlank()
     *
     */
    private $country;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return City
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return City
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return City
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return City
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return string
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set country
     *
     * @param \Plt\Bundle\LocationBundle\Entity\Country $country
     * @return City
     */
    public function setCountry(\Plt\Bundle\LocationBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Plt\Bundle\LocationBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
