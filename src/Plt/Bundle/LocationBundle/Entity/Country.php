<?php

namespace Plt\Bundle\LocationBundle\Entity;

use Algolia\AlgoliaSearchBundle\Mapping\Annotation as Algolia;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Country
 *
 * @ORM\Table(name="Country", indexes={
 *      @ORM\Index(name="country_country_code", columns={"country_code"}),
 *      @ORM\Index(name="country_slug", columns={"slug"})
 * })
 * @ORM\Entity
 *
 * Algolia\Index(attributesToIndex = {"country_code", "slug"})
 */
class Country
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="country_code", type="string", length=4)
     * @Assert\NotBlank()
     *
     * Algolia\Attribute
     */
    private $countryCode;

    /**
     * @ORM\Column(name="name", type="string", length=64)
     * @Assert\NotBlank()
     *
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", length=64)
     * @Assert\NotBlank()
     *
     * Algolia\Attribute
     */
    private $slug;

    /**
     * @ORM\Column(name="iso_numeric", type="string", length=64)
     * @Assert\NotBlank()
     *
     */
    private $isoNumeric;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UtilityBundle\Entity\Currency")
     */
    private $currency;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return Country
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Country
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set isoNumeric
     *
     * @param string $isoNumeric
     * @return Country
     */
    public function setIsoNumeric($isoNumeric)
    {
        $this->isoNumeric = $isoNumeric;

        return $this;
    }

    /**
     * Get isoNumeric
     *
     * @return string
     */
    public function getIsoNumeric()
    {
        return $this->isoNumeric;
    }

    /**
     * Set currency
     *
     * @param \Plt\Bundle\UtilityBundle\Entity\Currency $currency
     * @return Country
     */
    public function setCurrency(\Plt\Bundle\UtilityBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Plt\Bundle\UtilityBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
