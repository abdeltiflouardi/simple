<?php

namespace Plt\Bundle\LocationBundle\Manager;

use Plt\Bundle\LocationBundle\Entity\City;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class CityManager extends EntityManager
{
    public function getCitiesQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('c');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getCities($searchQuery, $limit, $offset)
    {
        $cityQueryBuilder = $this->getCitiesQueryBuilder($limit, $offset);

        if (null != $searchQuery) {
            $cityQueryBuilder->where(
                $cityQueryBuilder->expr()->like('c.name', $cityQueryBuilder->expr()->literal(sprintf('%%%s%%', $searchQuery)))
            );
        }

        return $cityQueryBuilder->getQuery()->execute();
    }

    public function getCitiesByCountry($country, $limit, $offset)
    {
        $cityQueryBuilder = $this->getCitiesQueryBuilder($limit, $offset);

        $cityQueryBuilder
            ->where('c.country = :country')
            ->setParameter('country', VarUtil::toInt($country))
        ;

        return $cityQueryBuilder->getQuery()->execute();
    }

    public function addCity($mixed, $country)
    {
        $mixed['country'] = $country;

        $city = new City();

        ArrayUtil::toEntity($city, $mixed);

        $errors = $this->validate($city);
        if (null != $errors) {
            return $errors;
        }

        $this->save($city);

        return $city;
    }

    public function search($text)
    {

    }
}
