<?php

namespace Plt\Bundle\LocationBundle\Manager;

use Plt\Bundle\LocationBundle\Entity\Country;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class CountryManager extends EntityManager
{
    public function getCountriesQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('c');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getCountries($searchQuery, $limit = 10, $offset = 0)
    {
        $countryQueryBuilder = $this->getCountriesQueryBuilder($limit, $offset);

        if (null != $searchQuery) {
            $countryQueryBuilder->where(
                $countryQueryBuilder->expr()->like('c.name', $countryQueryBuilder->expr()->literal(sprintf('%%%s%%', $searchQuery)))
            );
        }

        return $countryQueryBuilder->getQuery()->execute();
    }

    public function addCountry($mixed)
    {
        $country = new Country();

        ArrayUtil::toEntity($country, $mixed);

        $errors = $this->validate($country);
        if (null != $errors) {
            return $errors;
        }

        $this->save($country);

        return $country;
    }

    public function search($text)
    {

    }
}
