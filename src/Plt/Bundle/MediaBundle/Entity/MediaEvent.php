<?php

namespace Plt\Bundle\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaEvent
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MediaEvent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=32)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\MediaBundle\Entity\Media")
     */
    private $media;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EventBundle\Entity\Event")
     */
    private $event;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MediaEvent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set media
     *
     * @param \Plt\Bundle\MediaBundle\Entity\Media $media
     * @return MediaEvent
     */
    public function setMedia(\Plt\Bundle\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Plt\Bundle\MediaBundle\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set event
     *
     * @param \Plt\Bundle\EventBundle\Entity\Event $event
     * @return MediaEvent
     */
    public function setEvent(\Plt\Bundle\EventBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \Plt\Bundle\EventBundle\Entity\Event 
     */
    public function getEvent()
    {
        return $this->event;
    }
}
