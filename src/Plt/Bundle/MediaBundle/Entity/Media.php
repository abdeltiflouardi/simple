<?php

namespace Plt\Bundle\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Media
 *
 * @ORM\Table(name="Media", indexes={
 *      @ORM\Index(name="media_hash", columns={"hash"})
 * })
 * @ORM\Entity
 */
class Media
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=32)
     */
    private $name;

    /**
     * @ORM\Column(name="hash", type="string", length=128)
     */
    private $hash;

    /**
     * JPG|PNG|GIF|AVI|MP4
     *
     * @ORM\Column(name="extension", type="string", length=4)
     */
    private $extension;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\MediaBundle\Entity\MediaType")
     */
    private $mediaType;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Media
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return Media
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set extension
     *
     * @param string $extension
     * @return Media
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set mediaType
     *
     * @param \Plt\Bundle\MediaBundle\Entity\MediaType $mediaType
     * @return Media
     */
    public function setMediaType(\Plt\Bundle\MediaBundle\Entity\MediaType $mediaType = null)
    {
        $this->mediaType = $mediaType;

        return $this;
    }

    /**
     * Get mediaType
     *
     * @return \Plt\Bundle\MediaBundle\Entity\MediaType
     */
    public function getMediaType()
    {
        return $this->mediaType;
    }
}
