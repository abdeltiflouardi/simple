<?php

namespace Plt\Bundle\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaType
 *
 * @ORM\Table(name="MediaType", indexes={
 *      @ORM\Index(name="media_type_slug", columns={"slug"}),
 *      @ORM\Index(name="media_type_type", columns={"type"}),
 * })
 * @ORM\Entity
 */
class MediaType
{
    const MediaTypeVideo = 'VIDEO';
    const MediaTypePicture = 'PICTURE';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=32)
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", length=32)
     */
    private $slug;

    /**
     * @ORM\Column(name="height", type="integer", length=4)
     */
    private $height;

    /**
     * @ORM\Column(name="width", type="integer", length=4)
     */
    private $width;

    /**
     * @ORM\Column(name="type", type="string", length=16)
     */
    private $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MediaType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return MediaType
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return MediaType
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return MediaType
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return MediaType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
