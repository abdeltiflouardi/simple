<?php

namespace Plt\Bundle\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaCity
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MediaCity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=32)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\MediaBundle\Entity\Media")
     */
    private $media;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\LocationBundle\Entity\City")
     */
    private $city;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MediaCity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set media
     *
     * @param \Plt\Bundle\MediaBundle\Entity\Media $media
     * @return MediaCity
     */
    public function setMedia(\Plt\Bundle\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Plt\Bundle\MediaBundle\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set city
     *
     * @param \Plt\Bundle\LocationBundle\Entity\City $city
     * @return MediaCity
     */
    public function setCity(\Plt\Bundle\LocationBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Plt\Bundle\LocationBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }
}
