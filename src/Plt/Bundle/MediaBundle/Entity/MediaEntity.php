<?php

namespace Plt\Bundle\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaEntity
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MediaEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=32)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\MediaBundle\Entity\Media")
     */
    private $media;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EntityBundle\Entity\Entity")
     */
    private $entity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MediaEntity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set media
     *
     * @param \Plt\Bundle\MediaBundle\Entity\Media $media
     * @return MediaEntity
     */
    public function setMedia(\Plt\Bundle\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Plt\Bundle\MediaBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set entity
     *
     * @param \Plt\Bundle\ShareableBundle\Entity\Entity $entity
     * @return MediaEntity
     */
    public function setEntity(\Plt\Bundle\ShareableBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Plt\Bundle\ShareableBundle\Entity\Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }
}
