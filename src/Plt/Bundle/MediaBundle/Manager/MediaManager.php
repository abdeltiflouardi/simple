<?php

namespace Plt\Bundle\MediaBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;

class MediaManager extends EntityManager
{
    public function addMedia($mixed)
    {

    }

    public function deleteMedia($media, $user)
    {

    }

    public function modifyMedia($media, $user)
    {

    }

    public function associateMediaShareable($media, $user)
    {

    }

    public function associateMediaEvent($media, $user)
    {

    }

    public function associateMediaCountry($media, $user)
    {

    }

    public function associateMediaCity($media, $user)
    {

    }

    public function dissociateMediaShareable($media, $user)
    {

    }

    public function dissociateMediaEvent($media, $user)
    {

    }

    public function dissociateMediaCountry($media, $user)
    {

    }

    public function dissociateMediaCity($media, $user)
    {

    }
}
