<?php

namespace Plt\Bundle\MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Message
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     */
    private $userFrom;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     */
    private $userTo;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\MessageBundle\Entity\Message")
     */
    private $replayTo;

    /**
     * @ORM\ManyToOne(targetEntity="Plt\Bundle\ReviewBundle\Entity\Review")
     */
    private $review;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set review
     *
     * @param \Plt\Bundle\ReviewBundle\Entity\Review $review
     * @return Message
     */
    public function setReview(\Plt\Bundle\ReviewBundle\Entity\Review $review = null)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return \Plt\Bundle\ReviewBundle\Entity\Review
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Message
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Message
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set userFrom
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $userFrom
     * @return Message
     */
    public function setUserFrom(\Plt\Bundle\UserBundle\Entity\User $userFrom = null)
    {
        $this->userFrom = $userFrom;

        return $this;
    }

    /**
     * Get userFrom
     *
     * @return \Plt\Bundle\UserBundle\Entity\User 
     */
    public function getUserFrom()
    {
        return $this->userFrom;
    }

    /**
     * Set userTo
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $userTo
     * @return Message
     */
    public function setUserTo(\Plt\Bundle\UserBundle\Entity\User $userTo = null)
    {
        $this->userTo = $userTo;

        return $this;
    }

    /**
     * Get userTo
     *
     * @return \Plt\Bundle\UserBundle\Entity\User 
     */
    public function getUserTo()
    {
        return $this->userTo;
    }

    /**
     * Set replayTo
     *
     * @param \Plt\Bundle\MessageBundle\Entity\Message $replayTo
     * @return Message
     */
    public function setReplayTo(\Plt\Bundle\MessageBundle\Entity\Message $replayTo = null)
    {
        $this->replayTo = $replayTo;

        return $this;
    }

    /**
     * Get replayTo
     *
     * @return \Plt\Bundle\MessageBundle\Entity\Message 
     */
    public function getReplayTo()
    {
        return $this->replayTo;
    }
}
