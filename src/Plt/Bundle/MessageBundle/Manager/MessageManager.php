<?php

namespace Plt\Bundle\MessageBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;

class MessageManager extends EntityManager
{
    public function sendMessage($userFrom, $userTo, $mixed)
    {

    }

    public function removeMessage($message, $user)
    {

    }

    public function replyToMessage($userFrom, $message)
    {

    }
}
