<?php

namespace Plt\Bundle\AsyncBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NameEventCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('plt:async')
            ->setDescription('type de commande à lancer en attente')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Not yet implemented!');
    }
}
