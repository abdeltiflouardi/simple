<?php

namespace Plt\Bundle\EventBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;

class EventManager extends EntityManager
{

    public function getEventsQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('e');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function toggleEventLive($user, $event)
    {
        // @todo
    }

    public function getEventsByCity($city, $limit = 10, $offset = 0)
    {
        // @todo
    }

    public function getEventsByUser($user, $limit = 10, $offset = 0)
    {
        // @todo
    }

    public function getEventsByParticipant($user, $limit = 10, $offset = 0)
    {
        // @todo
    }

    public function getEventsByDate($minDate, $maxDate)
    {
        // @todo
    }

    public function getEventsBy($parameters)
    {
        // @todo
    }

    public function createEvent($user, $mixed)
    {
        // @todo
    }

    public function addParticipant($user, $event)
    {
        // @todo
    }

    public function addEntity($entity, $event)
    {
        // @todo
    }

    public function removeParticipant($user, $event)
    {
        // @todo
    }

    public function removeEntity($entity, $event)
    {
        // @todo
    }

    public function updateEvent($user, $event)
    {
        // @todo
    }
}
