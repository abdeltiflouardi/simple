<?php

namespace Plt\Bundle\EventBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;

class EventEntityManager extends EntityManager
{

    public function getEventsEntityQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('e');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getEventEntities($event)
    {
        // @todo
    }

    public function createEventEntity($entity, $event, $status)
    {
        // @todo
    }

    public function deleteEventEntity($entity, $event)
    {
        // @todo
    }
}
