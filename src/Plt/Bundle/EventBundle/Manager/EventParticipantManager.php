<?php

namespace Plt\Bundle\EventBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;

class EventParticipantManager extends EntityManager
{

    public function getEventsParticipantQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('e');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function createParticipant($user, $event, $status)
    {
        // @todo
    }

    public function deleteParticipant($user, $event)
    {
        // @todo
    }

    public function setParticipantStatus($user, $event, $status)
    {
        // @todo
    }

    public function getEventParticipantsByStatus($event, $status)
    {
        // @todo
    }
}
