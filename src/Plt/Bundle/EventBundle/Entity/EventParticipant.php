<?php

namespace Plt\Bundle\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventParticipant
 *
 * @ORM\Table(name="EventParticipant", indexes={
 *      @ORM\Index(name="event_participant_status", columns={"status"})
 * })
 * @ORM\Entity
 */
class EventParticipant
{

    const INVITED = 'INVITED';
    const ATTENDING = 'ATTENDING';
    const NOT_ATTENDING = 'NOT_ATTENDING';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="string", length=32)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EventBundle\Entity\Event")
     */
    private $event;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return EventParticipant
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $user
     * @return EventParticipant
     */
    public function setUser(\Plt\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set event
     *
     * @param \Plt\Bundle\EventBundle\Entity\Event $event
     * @return EventParticipant
     */
    public function setEvent(\Plt\Bundle\EventBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \Plt\Bundle\EventBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }
}
