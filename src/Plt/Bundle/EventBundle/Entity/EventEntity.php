<?php

namespace Plt\Bundle\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventEntity
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class EventEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="string", length=32)
     *
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EntityBundle\Entity\Entity")
     *
     */
    private $entity;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EventBundle\Entity\Event")
     *
     */
    private $event;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entity
     *
     * @param \Plt\Bundle\EntityBundle\Entity\Entity $entity
     * @return EventEntity
     */
    public function setEntity(\Plt\Bundle\EntityBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Plt\Bundle\EntityBundle\Entity\Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set event
     *
     * @param \Plt\Bundle\EventBundle\Entity\Event $event
     * @return EventEntity
     */
    public function setEvent(\Plt\Bundle\EventBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \Plt\Bundle\EventBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return EventEntity
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
