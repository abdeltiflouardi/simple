<?php

namespace Plt\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PltUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
