<?php

namespace TBM\Bundle\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Plt\Bundle\UserBundle\Entity\Address;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadAddressData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $address = new Address();
        $address
            ->setUser($this->getReference('user'))
            ->setName('Default')
            ->setNumber('14 bis')
            ->setRoad('Paris')
            ->setApt('')
            ->setMore('')
            ->setCode('72000')
        ;

        $manager->persist($address);
        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
