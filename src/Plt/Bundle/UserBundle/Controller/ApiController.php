<?php

namespace Plt\Bundle\UserBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Plt\Bundle\ApiBundle\Controller\BaseApiController as Controller;
use Plt\Bundle\EntityBundle\Entity\Entity;
use Plt\Bundle\LocationBundle\Entity\City;
use Plt\Bundle\LocationBundle\Entity\Country;
use Plt\Bundle\UserBundle\Entity\Address;
use Plt\Bundle\UserBundle\Entity\ListElement;
use Plt\Bundle\UserBundle\Entity\Lists;
use Plt\Bundle\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{

    /**
     * return \Plt\Bundle\UserBundle\Manager\AddressManager
     */
    public function getAddressManager()
    {
        return $this->container->get('plt_address.manager');
    }

    /**
     * return \Plt\Bundle\UserBundle\Manager\UserManager
     */
    public function getUserManager()
    {
        return $this->container->get('plt_user.manager');
    }

    /**
     * return \Plt\Bundle\UserBundle\Manager\ListsManager
     */
    public function getListsManager()
    {
        return $this->container->get('plt_lists.manager');
    }

    /**
     * return \Plt\Bundle\UserBundle\Manager\ListElementManager
     */
    public function getListElementManager()
    {
        return $this->container->get('plt_list_element.manager');
    }

    /**
     * return \Plt\Bundle\UserBundle\Manager\FollowingManager
     */
    public function getFollowingManager()
    {
        return $this->container->get('plt_following.manager');
    }

    /**
     * List all users.
     *
     * @ApiDoc(
     *     section="/users",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of Users.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="10", description="How many users to return.")
     *
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return \Plt\Bundle\UserBundle\Entity\User[]
     */
    public function getUsersAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getUserManager()->getUsers($limit, $offset);
    }

    /**
     * Create follow users
     *
     * @ApiDoc(
     *     section="/users",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\Post("/users/{user_id}/follow/{to_user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     * @ParamConverter("userTo", class="PltUserBundle:User", options={"id": "to_user_id"})
     *
     * @return RouteRedirectView
     */
    public function postUsersFollowsAction($user, $userTo)
    {
        $following = $this->getFollowingManager()->addFollowing($user, $userTo);

        return $following;
    }

    /**
     * Delete follow users
     *
     * @ApiDoc(
     *     section="/users",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\Delete("/users/{user_id}/follow/{to_user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     * @ParamConverter("userTo", class="PltUserBundle:User", options={"id": "to_user_id"})
     *
     * @return RouteRedirectView
     */
    public function deleteUsersFollowsAction($user, $userTo)
    {
        $this->getFollowingManager()->deleteFollowing($user, $userTo);
    }

    /**
     * Create a new user.
     *
     * @ApiDoc(
     *   section="/users",
     *   statusCodes={
     *     200="Returned when successful",
     *     400="Returned when the form has errors"
     *   }
     * )
     *
     * @return RouteRedirectView
     */
    public function postUsersAction(Request $request)
    {
        $data = json_decode($request->get('data'));

        $user = $this->getUserManager()->addUsers($data);

        return $user;
    }

    /**
     * Get addresses by User.
     *
     * @ApiDoc(
     *     section="/addresses",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the user is not found"
     *     },
     *     requirements={
     *         {"name"="user_id", "dataType"="integer", "requirement"="\d+", "description"="user id"}
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list addresses.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="10", description="How many addresses to return.")
     *
     * @Annotations\Get("/addresses/users/{user_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     *
     * @param User                  $user           the user object
     * @param ParamFetcherInterface $paramFetcher   param fetcher service
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address[]
     */
    public function getAddressesUserAction(User $user, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getAddressManager()->getAddressesByUser($user, $limit, $offset);
    }

    /**
     * Get addresses by Country.
     *
     * @ApiDoc(
     *     section="/addresses",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     },
     *     requirements={
     *         {"name"="country_id", "dataType"="integer", "requirement"="\d+", "description"="country id"}
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list addresses.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many addresses to return.")
     *
     * @Annotations\Get("/addresses/countries/{country_id}")
     * @Annotations\View()
     *
     * @ParamConverter("country", class="PltLocationBundle:Country", options={"id": "country_id"})
     *
     * @param Country               $country        the country object
     * @param ParamFetcherInterface $paramFetcher   param fetcher service
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address[]
     */
    public function getAddressesCountryAction(Country $country, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getAddressManager()->getAddressesByCountry($country, $limit, $offset);
    }

    /**
     * Get addresses by User and Country.
     *
     * @ApiDoc(
     *     section="/addresses",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     },
     *     requirements={
     *         {"name"="user_id", "dataType"="integer", "requirement"="\d+", "description"="user id"},
     *         {"name"="country_id", "dataType"="integer", "requirement"="\d+", "description"="country id"}
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default="1", description="Page of list addresses.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="10", description="How many addresses to return.")
     *
     * @Annotations\Get("/addresses/users/{user_id}/countries/{country_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     * @ParamConverter("country", class="PltLocationBundle:Country", options={"id": "country_id"})
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address[]
     */
    public function getAddressesUserCountryAction(User $user, Country $country, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getAddressManager()->getAddressesByUserAndCountry($user, $country, $limit, $offset);
    }

    /**
     * Get addresses by User and city.
     *
     * @ApiDoc(
     *     section="/addresses",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     },
     *     requirements={
     *         {"name"="user_id", "dataType"="integer", "requirement"="\d+", "description"="user id"},
     *         {"name"="city_id", "dataType"="integer", "requirement"="\d+", "description"="city id"}
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list addresses.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="10", description="How many addresses to return.")
     *
     * @Annotations\Get("/addresses/users/{user_id}/cities/{city_id}")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     * @ParamConverter("city", class="PltLocationBundle:City", options={"id": "city_id"})
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address[]
     */
    public function getAddressesUserCityAction(User $user, City $city, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getAddressManager()->getAddressesByUserAndCity($user, $city, $limit, $offset);
    }

    /**
     * List all addresses.
     *
     * @ApiDoc(
     *     section="/addresses",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list addresses.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="10", description="How many addresses to return.")
     *
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address[]
     */
    public function getAddressesAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getAddressManager()->getAddresses($limit, $offset);
    }

    /**
     * Get Address.
     *
     * @ApiDoc(
     *     section="/addresses",
     *     output = "\Plt\Bundle\UserBundle\Entity\Address",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the address not found"
     *     },
     *     requirements={
     *         {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="address id"}
     *     }
     * )
     *
     * @Annotations\Get("/addresses/{id}")
     *
     * @return \Plt\Bundle\UserBundle\Entity\Address
     */
    public function getAddressAction(Address $address)
    {
        return $address;
    }

    /**
     * Create a new address.
     *
     * @ApiDoc(
     *   section="/addresses",
     *   statusCodes={
     *     200="Returned when successful",
     *     400="Returned when the form has errors"
     *   }
     * )
     *
     * @return RouteRedirectView
     */
    public function postAddressesAction(Request $request)
    {
        $user = $this->getUser();
        $data = $request->get('data');

        $address = $this->getAddressManager()->addAddress($data, $user);

        return $address;
    }

    /**
     * Update existing address.
     *
     * @ApiDoc(
     *   section="/addresses",
     *   statusCodes={
     *     204="Returned when successful",
     *     400="Returned when the form has errors",
     *     403="Returned when the user is not authorized"
     *   },
     *   requirements={
     *     {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="address id"}
     *   }
     * )
     *
     * @Annotations\Put("/addresses/{id}")
     *
     * @return RouteRedirectView
     */
    public function putAddressesAction(Request $request, Address $address)
    {
        $user = $this->getUser();
        $data = $request->get('data');

        $addressResponse = $this->getAddressManager()->updateAddress($address, $data, $user);

        return $addressResponse;
    }

    /**
     * Removes a address.
     *
     * @ApiDoc(
     *   section="/addresses",
     *   statusCodes={
     *     204="Returned when successful",
     *     403="Returned when the user is not authorized"
     *   },
     *   requirements={
     *     {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="address id"}
     *   }
     * )
     *
     * @Annotations\Delete("/addresses/{id}")
     *
     * @return RouteRedirectView
     */
    public function deleteAddressesAction(Address $address)
    {
        $user = $this->getUser();
        $this->getAddressManager()->deleteAddress($address, $user);

        return array();
    }

    /**
     * Get list by User.
     *
     * @ApiDoc(
     *     section="/users",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the user is not found"
     *     },
     *     requirements={
     *         {"name"="user_id", "dataType"="integer", "requirement"="\d+", "description"="user id"}
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list addresses.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="10", description="How many addresses to return.")
     *
     * @Annotations\Get("/users/{user_id}/lists")
     * @Annotations\View()
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     *
     * @param User                  $user           the user object
     * @param ParamFetcherInterface $paramFetcher   param fetcher service
     *
     * @return \Plt\Bundle\UserBundle\Entity\Lists[]
     */
    public function getListsUserAction(User $user, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getListsManager()->getListsByUser($user, $limit, $offset);
    }

    /**
     * Create a new list to given user.
     *
     * @ApiDoc(
     *   section="/users",
     *   output="Plt\Bundle\UserBundle\Entity\Lists",
     *   statusCodes={
     *     200="Returned when successful",
     *     400="Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\Post("/users/{user_id}/lists")
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     *
     * @return RouteRedirectView
     */
    public function postUsersListsAction(Request $request, User $user)
    {
        $data = $request->get('data');
        $lists = $this->getListsManager()->addListForUser($user, $data);

        return $lists;
    }

    /**
     * Removes a list.
     *
     * @ApiDoc(
     *   section="/users",
     *   statusCodes={
     *     204="Returned when successful",
     *     404="Returned when the user or the list is not found"
     *   }
     * )
     *
     * @Annotations\Delete("/users/{user_id}/lists/{lists_id}")
     *
     * @ParamConverter("list", class="PltUserBundle:Lists", options={"id": "lists_id"})
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     *
     * @return RouteRedirectView
     */
    public function deleteUsersListsAction(User $user, Lists $list)
    {
        $this->getListsManager()->deleteListById($list, $user);
    }

    /**
     * Get list element by list.
     *
     * @ApiDoc(
     *     section="/users",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the user is not found"
     *     },
     *     requirements={
     *         {"name"="user_id", "dataType"="integer", "requirement"="\d+", "description"="user id"}
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of list elements.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="10", description="How many elements to return.")
     *
     * @Annotations\Get("/users/{user_id}/lists/{list_id}")
     * @Annotations\View()
     *
     * @ParamConverter("list", class="PltUserBundle:Lists", options={"id": "list_id"})
     *
     * @param ParamFetcherInterface $paramFetcher   param fetcher service
     *
     * @return \Plt\Bundle\UserBundle\Entity\ListElement[]
     */
    public function getListElementAction(Lists $list, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getListElementManager()->getListElementByList($list, $limit, $offset);
    }

    /**
     * Create a new list element.
     *
     * @ApiDoc(
     *   section="/users",
     *   output="Plt\Bundle\UserBundle\Entity\ListElement",
     *   statusCodes={
     *     200="Returned when successful",
     *     400="Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\Post("/users/{user_id}/lists/{list_id}/element/{entity_id}")
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     * @ParamConverter("list", class="PltUserBundle:Lists", options={"id": "list_id"})
     * @ParamConverter("entity", class="PltEntityBundle:Entity", options={"id": "entity_id"})
     *
     * @return RouteRedirectView
     */
    public function postUsersListElementAction(User $user, Lists $lists, Entity $entity)
    {
        $listElement = $this->getListElementManager()->addListElementByList($lists, $entity, $user);

        return $listElement;
    }

    /**
     * Removes an element list.
     *
     * @ApiDoc(
     *   section="/users",
     *   statusCodes={
     *     204="Returned when successful",
     *     404="Returned when the list element is not found"
     *   }
     * )
     *
     * @Annotations\Delete("/users/{user_id}/lists/element/{list_element_id}")
     *
     * @ParamConverter("listElement", class="PltUserBundle:ListElement", options={"id": "list_element_id"})
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     *
     * @return RouteRedirectView
     */
    public function deleteListElementAction(User $user, ListElement $listElement)
    {
        $this->getListElementManager()->deleteListElementById($listElement, $user);
    }
}
