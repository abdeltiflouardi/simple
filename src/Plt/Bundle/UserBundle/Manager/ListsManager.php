<?php

namespace Plt\Bundle\UserBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class ListsManager extends EntityManager
{
    public function getListsQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('l');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getListsByUser($user, $limit = 10, $offset = 0)
    {
        $listsQueryBuilder = $this->getListsQueryBuilder($limit, $offset);

        $listsQueryBuilder
            ->where('l.user = :user')
            ->setParameter('user', VarUtil::toInt($user))
        ;

        return $listsQueryBuilder->getQuery()->execute();
    }

    public function addListForUser($user, $mixed)
    {
        $mixed['user'] = $user;

        $lists = $this->createInstance();
        ArrayUtil::toEntity($lists, $mixed);

        $this->save($lists);

        return $lists;
    }

    public function deleteListById($lists, $user)
    {
        $this->delete($lists);
    }
}
