<?php

namespace Plt\Bundle\UserBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;
use Plt\Component\Util\VarUtil;

class AddressManager extends EntityManager
{

    public function getAddressesQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('a');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getAddresses($limit = 10, $offset = 0)
    {
        $addressQueryBuilder = $this->getAddressesQueryBuilder($limit, $offset);

        return $addressQueryBuilder->getQuery()->execute();
    }

    public function getAddressesByUser($user, $limit = 10, $offset = 0)
    {
        $addressQueryBuilder = $this->getAddressesQueryBuilder($limit, $offset);

        //
        $addressQueryBuilder
            ->where('a.user = :user')
            ->setParameter('user', VarUtil::toInt($user))
        ;

        return $addressQueryBuilder->getQuery()->execute();
    }

    public function getAddressesByCountry($country, $limit = 10, $offset = 0)
    {
        $addressQueryBuilder = $this->getAddressesQueryBuilder($limit, $offset);
        $addressQueryBuilder->join('a.city', 'c');

        //
        $addressQueryBuilder
            ->where('c.country = :country')
            ->setParameter('country', VarUtil::toInt($country))
        ;

        return $addressQueryBuilder->getQuery()->execute();
    }

    public function getAddressesByUserAndCountry($user, $country, $limit = 10, $offset = 0)
    {
        $addressQueryBuilder = $this->getAddressesQueryBuilder($limit, $offset);

        $addressQueryBuilder->join('a.city', 'c');

        //
        $addressQueryBuilder
            ->where('a.user = :user')
            ->setParameter('user', VarUtil::toInt($user))

            ->andWhere('c.country = :country')
            ->setParameter('country', VarUtil::toInt($country))
        ;

        return $addressQueryBuilder->getQuery()->execute();
    }

    public function getAddressesByUserAndCity($user, $city, $limit = 10, $offset = 0)
    {
        $addressQueryBuilder = $this->getAddressesQueryBuilder($limit, $offset);

        //
        $addressQueryBuilder
            ->where('a.user = :user')
            ->setParameter('user', VarUtil::toInt($user))

            ->andWhere('a.city = :city')
            ->setParameter('city', VarUtil::toInt($city))
        ;

        return $addressQueryBuilder->getQuery()->execute();
    }

    public function addAddress($mixed, $user)
    {
        $address = $this->createInstance();

        return $this->updateAddress($address, $mixed, $user);
    }

    public function updateAddress($address, $mixed, $user)
    {
        ArrayUtil::toEntity($address, (array) $mixed);

        $errors = $this->validate($address);
        if (null != $errors) {
            return $errors;
        }

        $this->save($address);

        return $address;
    }

    public function deleteAddressById($address, $user)
    {
        $this->delete($address);
    }

    public function getAddressById($id)
    {
        return $this->repository->find($id);
    }

    public function setAddressIsMain($address, $user)
    {
        $address->setIsMain(true);

        $this->save($address);
    }
}
