<?php

namespace Plt\Bundle\UserBundle\Manager;

use Plt\Bundle\UserBundle\Entity\Following;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\VarUtil;

class FollowingManager extends EntityManager
{
    public function getFollowingQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('f');

        $qb
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        return $qb;
    }

    public function addFollowing($userFrom, $userTo)
    {
        $following = new Following();
        $following
            ->setUserFrom($userFrom)
            ->setUserTo($userTo)
        ;

        $this->save($following);

        return $following;
    }

    public function deleteFollowing($userFrom, $userTo)
    {
        $followingQueryBuilder = $this->getFollowingQueryBuilder(1, 0);
        $followingQueryBuilder
            ->where('f.userFrom = :userFrom')
            ->setParameter('userFrom', VarUtil::toInt($userFrom))

            ->andWhere('f.userTo = :userTo')
            ->setParameter('userTo', VarUtil::toInt($userTo))
        ;

        $result = $followingQueryBuilder->getQuery()->execute();

        foreach ($result as $follow) {
            $this->delete($follow);
        }
    }
}
