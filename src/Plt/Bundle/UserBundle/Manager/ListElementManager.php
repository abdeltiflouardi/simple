<?php

namespace Plt\Bundle\UserBundle\Manager;

use Plt\Bundle\UserBundle\Entity\ListElement;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;

class ListElementManager extends EntityManager
{
    public function getListElementQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('l');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getListElementByList($lists, $limit = 10, $offset = 0)
    {
        $listQueryBuilder = $this->getListElementQueryBuilder($limit, $offset);

        $listQueryBuilder
            ->where('l.lists = :lists')
            ->setParameter('lists', $lists)
        ;

        return $listQueryBuilder->getQuery()->execute();
    }

    public function addListElementByList($lists, $entity, $user)
    {
        $listElement = new ListElement();
        $listElement
            ->setLists($lists)
            ->setEntity($entity)
            ->setUser($user)
        ;

        $this->save($listElement);

        return $listElement;
    }

    public function deleteListElementById($listElement, $user)
    {
        $this->delete($listElement);
    }
}
