<?php

namespace Plt\Bundle\UserBundle\Manager;

use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class UserManager extends EntityManager
{
    public function getUsersQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('u');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getUsers($limit, $offset)
    {
        $userQueryBuilder = $this->getUsersQueryBuilder($limit, $offset);

        return $userQueryBuilder->getQuery()->execute();
    }

    public function addUsers($data)
    {
        $user = $this->createInstance();
        ArrayUtil::toEntity($user, (array) $data);

        $errors = $this->validate($user);
        if (null != $errors) {
            return $errors;
        }

        try {
            $this->save($user);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

        return $user;
    }
}
