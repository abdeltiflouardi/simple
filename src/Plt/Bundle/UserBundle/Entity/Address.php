<?php

namespace Plt\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Address
 *
 * @ORM\Table(name="Address", indexes={
 *      @ORM\Index(name="address_code_city", columns={"code", "city_id"}),
 *      @ORM\Index(name="address_is_main", columns={"is_main"})
 * })
 * @ORM\Entity
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     *
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(name="number", type="string", length=10)
     *
     * @Assert\NotBlank()
     */
    private $number;

    /**
     * @ORM\Column(name="road", type="string", length=255)
     *
     * @Assert\NotBlank()
     */
    private $road;

    /**
     * @ORM\Column(name="apt", type="string", length=100)
     *
     * @Assert\NotBlank()
     */
    private $apt;

    /**
     * @ORM\Column(name="more", type="string", length=100, nullable=true)
     */
    private $more;

    /**
     * @ORM\Column(name="is_main", type="boolean")
     *
     * @Assert\Type(type="bool")
     */
    private $isMain;

    /**
     * @ORM\Column(name="code", type="string", length=10)
     *
     * @Assert\NotBlank()
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\LocationBundle\Entity\City")
     */
    private $city;

    public function __construct()
    {
        $this->isMain = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Address
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Address
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set road
     *
     * @param string $road
     * @return Address
     */
    public function setRoad($road)
    {
        $this->road = $road;

        return $this;
    }

    /**
     * Get road
     *
     * @return string
     */
    public function getRoad()
    {
        return $this->road;
    }

    /**
     * Set apt
     *
     * @param string $apt
     * @return Address
     */
    public function setApt($apt)
    {
        $this->apt = $apt;

        return $this;
    }

    /**
     * Get apt
     *
     * @return string
     */
    public function getApt()
    {
        return $this->apt;
    }

    /**
     * Set more
     *
     * @param string $more
     * @return Address
     */
    public function setMore($more)
    {
        $this->more = $more;

        return $this;
    }

    /**
     * Get more
     *
     * @return string
     */
    public function getMore()
    {
        return $this->more;
    }

    /**
     * Set isMain
     *
     * @param boolean $isMain
     * @return Address
     */
    public function setIsMain($isMain)
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * Get isMain
     *
     * @return boolean
     */
    public function getIsMain()
    {
        return $this->isMain;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Address
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set user
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $user
     * @return Address
     */
    public function setUser(\Plt\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set city
     *
     * @param \Plt\Bundle\LocationBundle\Entity\City $city
     * @return Address
     */
    public function setCity(\Plt\Bundle\LocationBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Plt\Bundle\LocationBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }
}
