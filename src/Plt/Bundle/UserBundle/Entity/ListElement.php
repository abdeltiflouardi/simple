<?php

namespace Plt\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListElement
 *
 * @ORM\Table(name="ListElement", indexes={
 *      @ORM\Index(name="list_element_entity_user", columns={"user_id", "entity_id"})
 * })
 * @ORM\Entity
 */
class ListElement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\Lists")
     */
    private $lists;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\EntityBundle\Entity\Entity")
     */
    private $entity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lists
     *
     * @param \Plt\Bundle\UserBundle\Entity\Lists $lists
     * @return ListElement
     */
    public function setLists(\Plt\Bundle\UserBundle\Entity\Lists $lists = null)
    {
        $this->lists = $lists;

        return $this;
    }

    /**
     * Get lists
     *
     * @return \Plt\Bundle\UserBundle\Entity\Lists
     */
    public function getLists()
    {
        return $this->lists;
    }

    /**
     * Set user
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $user
     * @return ListElement
     */
    public function setUser(\Plt\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Plt\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set entity
     *
     * @param \Plt\Bundle\EntityBundle\Entity\Entity $entity
     * @return ListElement
     */
    public function setEntity(\Plt\Bundle\EntityBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Plt\Bundle\EntityBundle\Entity\Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }
}
