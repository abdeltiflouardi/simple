<?php

namespace Plt\Bundle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApiToken
 *
 * @ORM\Table(name="ApiToken", indexes={
 *      @ORM\Index(name="api_token_token", columns={"token"})
 * })
 * @ORM\Entity
 */
class ApiToken
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="\Plt\Bundle\ApiBundle\Entity\ApiApplication")
     */
    private $app;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return ApiToken
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set user
     *
     * @param \Plt\Bundle\UserBundle\Entity\User $user
     * @return ApiToken
     */
    public function setUser(\Plt\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Plt\Bundle\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set app
     *
     * @param \Plt\Bundle\ApiBundle\Entity\ApiApplication $app
     * @return ApiToken
     */
    public function setApp(\Plt\Bundle\ApiBundle\Entity\ApiApplication $app = null)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * Get app
     *
     * @return \Plt\Bundle\ApiBundle\Entity\ApiApplication 
     */
    public function getApp()
    {
        return $this->app;
    }
}
