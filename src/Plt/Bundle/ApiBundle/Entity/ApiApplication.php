<?php

namespace Plt\Bundle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApiApplication
 *
 * @ORM\Table(name="ApiApplication", indexes={
 *      @ORM\Index(name="api_application_key", columns={"key"})
 * })
 * @ORM\Entity
 */
class ApiApplication
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="`key`", type="string", length=255)
     */
    private $key;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(name="login_redirect", type="text")
     */
    private $loginRedirect;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key
     *
     * @param string $key
     * @return ApiApplication
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ApiApplication
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set loginRedirect
     *
     * @param string $loginRedirect
     * @return ApiApplication
     */
    public function setLoginRedirect($loginRedirect)
    {
        $this->loginRedirect = $loginRedirect;

        return $this;
    }

    /**
     * Get loginRedirect
     *
     * @return string
     */
    public function getLoginRedirect()
    {
        return $this->loginRedirect;
    }
}
