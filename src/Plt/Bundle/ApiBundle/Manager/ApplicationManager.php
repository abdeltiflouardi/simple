<?php

namespace Plt\Bundle\ApiBundle\Manager;

use Plt\Bundle\ApiBundle\Entity\ApiApplication;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;
use Plt\Component\Util\ArrayUtil;

class ApplicationManager extends EntityManager
{
    public function getApplicationsQueryBuilder($limit = 10, $offset = 0)
    {
        $qb = $this->repository->createQueryBuilder('a');

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb;
    }

    public function getApplications($limit = 10, $offset = 0)
    {
        $appQueryBuilder = $this->getApplicationsQueryBuilder($limit, $offset);

        return $appQueryBuilder->getQuery()->execute();
    }

    public function addApplication($mixed)
    {
        $app = new ApiApplication();
        ArrayUtil::toEntity($app, $mixed);

        $errors = $this->validate($app);
        if (null != $errors) {
            return $errors;
        }

        $this->save($app);

        return $app;
    }
}
