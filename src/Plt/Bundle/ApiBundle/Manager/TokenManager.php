<?php

namespace Plt\Bundle\ApiBundle\Manager;

use Plt\Bundle\ApiBundle\Entity\ApiToken;
use Plt\Component\Doctrine\ORM\AbstractEntityManager as EntityManager;

class TokenManager extends EntityManager
{
    public function addToken($user, $app)
    {
        $token = $this->createToken($user, $app);

        $apiToken = new ApiToken();
        $apiToken
            ->setApp($app)
            ->setUser($user)
            ->setToken($token)
        ;

        $this->save($apiToken);

        return $apiToken;
    }

    public function createToken($user, $app, $size = 20)
    {
        $word = sprintf('%s%s%s%s', $user->getId(), $user->getPassword(), $app->getId(), $size);

        return md5($word);
    }
}
