<?php

namespace Plt\Bundle\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Plt\Bundle\ApiBundle\Controller\BaseApiController as Controller;
use Plt\Bundle\ApiBundle\Entity\ApiApplication;
use Plt\Bundle\ApiBundle\Entity\ApiToken;
use Plt\Bundle\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**
     * @return \Plt\Bundle\ApiBundle\Manager\ApplicationManager;
     */
    public function getApplicationManager()
    {
        return $this->container->get('plt_application.manager');
    }

    /**
     * @return \Plt\Bundle\ApiBundle\Manager\TokenManager;
     */
    public function getTokenManager()
    {
        return $this->container->get('plt_token.manager');
    }

    /**
     * List all applications.
     *
     * @ApiDoc(
     *     section="/applications",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", default=1, description="Page of applications.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default=10, description="How many applications to return.")
     *
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return \Plt\Bundle\ApiBundle\Entity\ApiApplication[]
     */
    public function getApplicationsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $offset = ($paramFetcher->get('page') - 1) * $limit;

        return $this->getApplicationManager()->getApplications($limit, $offset);
    }

    /**
     * Create a new application.
     *
     * @ApiDoc(
     *   section="/applications",
     *   statusCodes={
     *     200="Returned when successful",
     *     403="Returned when the user is not authorized"
     *   }
     * )
     *
     * @return RouteRedirectView
     */
    public function postApplicationsAction(Request $request)
    {
        $data = $request->get('data');

        $application = $this->getApplicationManager()->addApplication($data);

        return $application;
    }

    /**
     * Get token.
     *
     * @ApiDoc(
     *     section="/applications",
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized"
     *     }
     * )
     *
     * @Annotations\Get("/applications/tokens/{token_id}")
     * @Annotations\View()
     *
     * @ParamConverter("token", class="PltApiBundle:ApiToken", options={"id": "token_id"})
     *
     * @return \Plt\Bundle\ApiBundle\Entity\ApiToken
     */
    public function getApplicationsTokenAction(ApiToken $token)
    {
        return $token;
    }

    /**
     * Create a new token.
     *
     * @ApiDoc(
     *   section="/applications",
     *   statusCodes={
     *     200="Returned when successful",
     *     403="Returned when the user is not authorized"
     *   }
     * )
     *
     * @Annotations\Post("/applications/{application_id}/users/{user_id}/token")
     *
     * @ParamConverter("user", class="PltUserBundle:User", options={"id": "user_id"})
     * @ParamConverter("app", class="PltApiBundle:ApiApplication", options={"id": "application_id"})
     *
     * @return RouteRedirectView
     */
    public function postApplicationsUsersTokensAction(ApiApplication $app, User $user)
    {
        $token = $this->getTokenManager()->addToken($user, $app);

        return $token;
    }
}
