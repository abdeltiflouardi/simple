<?php

namespace Plt\Component\Doctrine;

interface ObjectManagerInterface
{
    public function createInstance();
    public function getObjectClassName();
}
