<?php

namespace Plt\Component\Doctrine\ORM;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Plt\Component\Doctrine\ObjectManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Description of AbstractEntityManager
 *
 */
abstract class AbstractEntityManager implements ObjectManagerInterface
{

    /**
     *
     * @var EntityManager
     */
    protected $em;

    /**
     *
     * @var EntityRepository
     */
    protected $repository;

    /**
     *
     * @var
     */
    protected $class;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     *
     */
    public function __construct(EntityManager $em, $class, EventDispatcherInterface $dispatcher = null)
    {
        $this->em = $em;
        $this->repository = $em->getRepository($class);
        $this->class = $class;

        $this->dispatcher = $dispatcher;
    }

    /**
     * @return string
     */
    public function getObjectClassName()
    {
        return $this->class;
    }

    /**
     *
     * @param object $entity
     */
    public function save($entity, $flush = true)
    {
        $this->em->persist($entity);

        if ($flush) {
            $this->em->flush();
        }
    }

    /**
     *
     * @param object $entity
     */
    public function delete($entity, $flush = true)
    {
        $this->em->remove($entity);

        if ($flush) {
            $this->em->flush();
        }
    }

    public function createInstance()
    {
        return new $this->class;
    }

    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($entity)
    {
        if (!$this->validator instanceof ValidatorInterface) {
            throw new \Exception("There is no validator configured.", 1);
        }

        $errors = $this->validator->validate($entity);
        if (count($errors) > 0) {
            return $errors;
        }

        return null;
    }
}
