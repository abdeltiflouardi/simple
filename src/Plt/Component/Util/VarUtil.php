<?php

namespace Plt\Component\Util;

class VarUtil
{
    public static function toInt($var)
    {
        if (is_object($var)) {
            return $var->getId();
        } else {
            return (int) $var;
        }
    }
}
