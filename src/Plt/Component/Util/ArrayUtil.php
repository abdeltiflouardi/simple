<?php

namespace Plt\Component\Util;

class ArrayUtil
{
    public static function toEntity($entity, $array)
    {
        foreach ((array) $array as $field => $value) {
            $field = StringUtil::camelize($field);

            $method = sprintf('set%s', $field);

            $class = get_class($entity);
            $rc = new \ReflectionClass($class);
            if ($rc->hasMethod($method)) {
                $rm = new \ReflectionMethod($class, $method);
                $rm->invokeArgs($entity, array($value));
            }
        }
    }
}
