<?php

namespace Plt\Component\Util;

class StringUtil
{
    public static function camelize($word)
    {
        return lcfirst(str_replace(" ", "", ucwords(strtr($word, "_-", "  "))));
    }
}
